# -*- coding: utf-8 -*-
"""
Created on Sat Jul 19 16:11:27 2014

@author: Chris Goldenstein 
"""
def Linestrength_Collector_NIST(args):

    import Linestrength_Function_v1 as SF
    import Species_Function_NIST_v1 as SpF
    import numpy 
    import math

    
    #inputs          = Species_Vector, T, vstart, vend, Xabs_Vector
    inputs          = args[0]
    kk              = args[1]
    Species_Vector  = inputs[0]
    Telec           = inputs[1]
    vstart          = inputs[2]
    vend            = inputs[3]
    MoleFracs       = inputs[4]
    

    Species         = Species_Vector[kk]
    MoleFrac        = MoleFracs[kk]
    
    h               = 6.626e-34         # J-s
    kb              = 1.38e-23          # J/molec-K
    c               = 2.99792458e10      # cm/s


    
    #v                                       = numpy.arange(vstart,vend,dv)      
    M, Q_T, SpecData, num_ions    = SpF.Species_Function_NIST_v1(Species,Telec,vstart,vend)
    #M, Q_To, Q_T, SpecData, num_isotopes    = SpF.Species_Function_Term_v1(Species,T,vstart,vend)
    if Species.find('2+')>0:
        thisIon = 3
    elif Species.find('+')>0:
        thisIon = 2
    else:
        thisIon = 1
    
    
    vo      = SpecData[0,2]
    ii      = 0
    istart  = 0
    while vo < vstart and ii < len(SpecData[:,0]):
        vo      = SpecData[ii,2]
        ii      = ii + 1
        istart  = ii
    
    iend    = istart    
    ii      = istart
    while vo < vend and ii < len(SpecData[:,0]):
        vo      = SpecData[ii,2]
        ii      = ii + 1
        iend    = ii    
    
    if ii == len(SpecData[:,0]):
        iend = iend-1
    
    length  = len(SpecData[:,2])


    

    
    nu 		 = []
    strength = []
    edouble  = []
    eupper   = []
    ionnum   = []
    glower   = []
    gupper   = []
    ind = 0
    bad_ion = 0
    
    for ii in range(0,length):
        ion = SpecData[ii,1]
        
        if ion > num_ions and bad_ion==0:    
            print (Species)            
            print ('ion out of range')

        if ion > num_ions:
            ion = num_ions
            bad_ion = 1
        if ion == thisIon:
        	vo      = SpecData[ii,2]  
        	EinA    = SpecData[ii,3]
        	E       = SpecData[ii,4]  
        	Eupper  = SpecData[ii,5] #Eupper
        	g1      = SpecData[ii,6]  
        	g2      = SpecData[ii,7]


        	lamb    = 1/vo
        	fi      = g1*numpy.exp(-h*c*E/(kb*Telec))/Q_T[ion-1]
        	S_T     = lamb**2/(8*numpy.pi)*101325.0/(kb*Telec)*(.01)**3*fi*EinA*g2/g1*(1-numpy.exp(-h*c*vo/(kb*Telec)))/c*MoleFrac
            

        	nu.append(vo)
        	strength.append(S_T)
        	edouble.append(E)
        	eupper.append(Eupper)
        	ionnum.append(-1)
        	glower.append(g1)
        	gupper.append(g2)
        	ind = ind+1
           
    strengthsZipped = zip(nu,strength,edouble,ionnum,eupper,glower,gupper)
    strengths = [{'nu':x, 'abs':y, 'edb':z, 'iso':a, 'gair':b, 'gslf':c, 'nair':d} for x,y,z,a,b,c,d in strengthsZipped]
    
    return strengths




