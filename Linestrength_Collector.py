# -*- coding: utf-8 -*-
"""
Created on Sat Jul 19 16:11:27 2014

@author: Chris Goldenstein 
"""
def Linestrength_Collector(args):

    import Linestrength_Function_v1 as SF
    import Species_Function_Term_v2 as SpF
    import numpy 
    import math

    
    #inputs          = Species_Vector, T, P, vstart, vend, isotope_include, xabs_vector
    inputs          = args[0]
    kk              = args[1]
    Species_Vector  = inputs[0]
    T               = inputs[1]
    P               = inputs[2]
    vstart          = inputs[3]
    vend            = inputs[4]
    isotope_include = inputs[5]
    MoleFracs       = inputs[6]

    Species         = Species_Vector[kk]
    MoleFrac        = MoleFracs[kk]
    


    
    #v                                       = numpy.arange(vstart,vend,dv)      
    M, Q_To, Q_T, SpecData, num_isotopes    = SpF.Species_Function_Term_v2(Species,T,vstart,vend,isotope_include)
    #M, Q_To, Q_T, SpecData, num_isotopes    = SpF.Species_Function_Term_v1(Species,T,vstart,vend)


    
    vo      = SpecData[0,2]
    ii      = 0
    istart  = 0
    while vo < vstart and ii < len(SpecData[:,0]):
        vo      = SpecData[ii,2]
        ii      = ii + 1
        istart  = ii


    
    iend    = istart    
    ii      = istart
    while vo < vend and ii < len(SpecData[:,0]):
        vo      = SpecData[ii,2]
        ii      = ii + 1
        iend    = ii    
    
    if ii == len(SpecData[:,0]):
        iend = iend-1
    
    length  = len(SpecData[:,2])


    
    bad_iso = 0
    numLine = 0
    nu = numpy.zeros(length)
    strength = numpy.zeros(length)
    edouble  = numpy.zeros(length)
    isonum   = numpy.zeros(length)
    G_air = numpy.zeros(length)
    G_self = numpy.zeros(length)
    n_air	 = numpy.zeros(length)
    print 'calculating linestrengths'
    for ii in range(0,length):
        isotope = SpecData[ii,1]

        if isotope > num_isotopes and bad_iso==0:    
            print ('isotope out of range')

        if isotope > num_isotopes:
            isotope = num_isotopes
            bad_iso = 1
            
        vo      = SpecData[ii,2]        
        E       = SpecData[ii,7]  
        S_To    = SpecData[ii,3]
        Gair    = SpecData[ii,5]
        Gabs    = SpecData[ii,6]
        nair    = SpecData[ii,8]  
        Dair    = SpecData[ii,9]
        nabs    = 0.75    
        mair    = 0.96

        GairT   = Gair*(296.0/T)**nair
        GabsT   = Gabs*(296.0/T)**nabs
        DairT   = Dair*(296.0/T)**mair

        vo      = vo + P*DairT        
        S_T     = SF.Linestrength_Function_v1(vo,E,S_To,Q_To[isotope-1],Q_T[isotope-1],T)
        nu[ii] = vo
        strength[ii] = S_T*MoleFrac
        edouble[ii]  = E
        isonum[ii]   = isotope
        G_air[ii]	 = Gair
        G_self[ii]	 = Gabs
        n_air[ii]	 = nair
        numLine = numLine+1

    
    strengthsZipped = zip(nu,strength,edouble,isonum,G_air,G_self,n_air)
    strengths = [{'nu':x, 'abs':y, 'edb':z, 'iso':a, 'gair':b, 'gslf':c, 'nair':d} for x,y,z,a,b,c,d in strengthsZipped]
    print 'done calculating linestrengths'
    
    return strengths




