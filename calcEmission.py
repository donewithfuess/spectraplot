def calcEmission(T,P,L,vstart,vend,dv,Species_Vector,Xabs_Vector):

	import Absorbance_Code_Function_v1 as AF
	import multiprocessing
	import numpy



######################################################################################################################
# Calc wingeval

	num_proc        = 1                     # Number of threads to perform calculations on
# User inputs
	wing_eval       = 1                   # Distance away form linecenter to stop simulation lineshape (cm^-1)
	if P<10:
		wing_eval=200.0
	elif P>100:
		wing_eval=2000.0
	else:
		wing_eval=P*20.0

	########################################################################################################################
	# Python does the rest
	v               = numpy.arange(vstart,vend,dv)
	num_species     = len(Species_Vector)
	length_v        = len(v)
	v_alpha         = numpy.zeros((length_v,num_species+1))
	v_alpha[:,0]    = v
	v_emiss         = numpy.zeros((length_v,num_species+1))
	v_emiss[:,0]    = v
	
	h               = 6.626e-34         # J-s
	kb              = 1.38e-23          # J/molec-K
	c               = 2.99792458e8            # m/s
	f               = v*c*100.0         # 1/s 
	Ibb             = numpy.divide(2*h*f**3, c**2) * numpy.divide(1, numpy.exp( numpy.divide(h*f, kb*T) ) - 1 ) # Watts per Hz per steradian per m^2
	Ibb             = Ibb*c*100.0       # Watts per steradian per cm^-1 per  m^2
	Ibb             = Ibb*(1.0/100.0)**2# Watts per steradian per cm^-1 per cm^2
	Ibb             = Ibb*1000.0*1000.0        # micro-Watts per steradian per cm^-1 per cm^2

	inputs          = Species_Vector, T, P, Xabs_Vector, L, dv, vstart, vend, wing_eval	
	output			= numpy.zeros((length_v,num_species))
	outputEmission	= numpy.zeros((length_v,num_species))
	outs			= numpy.zeros((length_v,num_species))
	outs = {}

	
	for ii in range(0,num_species):
		inputs          = Species_Vector, T, P, Xabs_Vector, L, dv, vstart, vend, wing_eval
		output[:,ii]    = AF.Absorbance_Code_Function_v1([inputs,ii])
		outputEmission[:,ii] = Ibb * (1-numpy.exp(-output[:,ii])) # same units as Ibb, removed  Ibb * (
		ans = zip(v,outputEmission[:,ii])
		#ans = zip(v,output[:,ii])
		ansWithKeys = [{'nu':x, 'abs':y} for x,y in ans]
		outs[ii] 		= ansWithKeys

	return outs

