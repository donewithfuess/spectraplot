# -*- coding: utf-8 -*-
"""
Created on Sat Jul 19 16:11:27 2014

@author: Chris Goldenstein 
"""

def Voigt_McLean_v1(v, vo, vd, vc, dv, wing_eval):
    import math
    import numpy as np
    

    #Parameters for approximating Voigt profile (from McLean)
    C1  = [-1.2150, 1.2359, -0.3085, 0.0210]
    C2  = [-1.3509, 0.3786, 0.5906, -1.1858]
    C3  = [-1.2150, -1.2359, -0.3085, -0.0210]
    C4  = [-1.3509, -0.3786, 0.5906, 1.1858]

    length      = len(v)
    Phi_V       = np.zeros(length)
    istart      = 1
    iend        = length
    
    ilr         = wing_eval/dv
    ileftvo     = (vo-v[1])/dv
    irightvo    = (vo-v[length-1])/dv
        
    
    if vo - wing_eval < v[1]:
        istart = 1
    else:
        istart = ileftvo-ilr
            
    if vo + wing_eval > v[length-1]:
        iend = length
    else:
        iend = irightvo+ilr   

    LD = vc/vd
    if LD < 0.001:            
        Phi_V[istart:iend]    = 2.0/vd * math.sqrt( math.log(2.0)/math.pi ) * np.exp(-4.0*math.log(2.0)*(( v[istart:iend] - vo )/vd)**2 )
    elif LD > 1000.0:
        Phi_V[istart:iend]    = 1.0/(2.0*math.pi) * vc/( ( v[istart:iend] - vo )**2 + (vc/2.0)**2 )
    else:    
        a           = math.sqrt(math.log(2.0))*vc/vd                 #Calculate Voigt 'a' parameter
        Phi_D_vo    = (2.0/vd)*math.sqrt(math.log(2.0)/math.pi)        #Calculate doppler line shape at vo
        w           = 2.0*math.sqrt(math.log(2.0)) * (v[istart:iend]-vo) / vd       #Voigt 'w' parameter

        Voigt1      = np.divide( ( C1[2] * (a - C1[0]) + C1[3] * (w - C1[1]) ) , ( (a - C1[0])**2 + (w - C1[1])**2 ) ) 
        Voigt2      = np.divide( ( C2[2] * (a - C2[0]) + C2[3] * (w - C2[1]) ) , ( (a - C2[0])**2 + (w - C2[1])**2 ) )    
        Voigt3      = np.divide( ( C3[2] * (a - C3[0]) + C3[3] * (w - C3[1]) ) , ( (a - C3[0])**2 + (w - C3[1])**2 ) )
        Voigt4      = np.divide( ( C4[2] * (a - C4[0]) + C4[3] * (w - C4[1]) ) , ( (a - C4[0])**2 + (w - C4[1])**2 ) )
        Voigt       = Voigt1 + Voigt2 + Voigt3 + Voigt4;

        Phi_V[istart:iend]       = Phi_D_vo * Voigt;    
    
    
    return Phi_V

