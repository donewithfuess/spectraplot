def calcEmissionNIST(T,P,L,vstart,vend,dv,Species_Vector,Xabs_Vector,gammaTo,n,Telec):

	import Absorbance_Code_NIST_Function_v1 as NAF
	import numpy



######################################################################################################################
# User inputs
	
	num_proc        = 1                     # Number of threads to perform calculations on
	wing_eval       = 1                   # Distance away form linecenter to stop simulation lineshape (cm^-1)
	if P<0.5:
		wing_eval=2
	elif P<5:
		wing_eval=P*10.0
	else:
		wing_eval=50
	


	########################################################################################################################
	# Python does the rest
	v               = numpy.arange(vstart,vend,dv)
	num_species     = len(Species_Vector)
	length_v        = len(v)

	h               = 6.626e-34         # J-s
	kb              = 1.38e-23          # J/molec-K
	c               = 2.99792458e8            # m/s
	f               = v*c*100.0         # 1/s 
	Ibb             = numpy.divide(2*h*f**3, c**2) * numpy.divide(1, numpy.exp( numpy.divide(h*f, kb*Telec) ) - 1 ) # Watts per Hz per steradian per m^2
	Ibb             = Ibb*c*100.0       # Watts per steradian per cm^-1 per  m^2
	Ibb             = Ibb*(1.0/100.0)**2# Watts per steradian per cm^-1 per cm^2
	Ibb             = Ibb*1000.0*1000.0        # micro-Watts per steradian per cm^-1 per cm^2

	output			= numpy.zeros((length_v,num_species))
	outputEmission	= numpy.zeros((length_v,num_species))
	EinAFlag		= {}
	inputs          = Species_Vector, T, P, Xabs_Vector, L, dv, vstart, vend, wing_eval, gammaTo, n, Telec
	outs = {}

	for ii in range(0,num_species):	
		inputs          = Species_Vector, T, P, Xabs_Vector, L, dv, vstart, vend, wing_eval, gammaTo, n, Telec
		output[:,ii],EinsteinAFlag    = NAF.Absorbance_Code_NIST_Function_v1([inputs,ii])  
		outputEmission[:,ii] = Ibb * (1-numpy.exp(-output[:,ii])) # same units as Ibb, removed  Ibb * (
		ans = zip(v,outputEmission[:,ii])
		ansWithKeys = [{'nu':x, 'abs':y} for x,y in ans]
		outs[ii] 		= ansWithKeys
		
		EinAFlag[ii]=EinsteinAFlag
		
	return outs,EinAFlag