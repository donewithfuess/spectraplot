# -*- coding: utf-8 -*-
"""
Created on Tue Mar 17 14:42:34 2015

@author: Chris
"""

def Species_Function_NIST_v1(Species,T,vstart,vend):
    import numpy
    import Q_T_Function_NIST_v1 as QT
    import mysql.connector

    filepath    = "./static/NIST/"
    filepath    = "https://s3-us-west-1.amazonaws.com/spectraplot/NIST/"

    #remove any ion text, + or 2+, from species name
    Species = Species.translate(None,'2+')
    
    cnx = mysql.connector.connect(user='spectraplot', password='password', host='spectraplot.cq60ipps6rt2.us-west-1.rds.amazonaws.com', port='3306', database='NIST')
    cursor = cnx.cursor()   
    query = ("SELECT * FROM `"+Species+"` WHERE WAVENUMBER BETWEEN "+str(vstart-2.5)+ " AND "+str(vend+2.5)+";")
    
    cursor.execute(query)
    results = cursor.fetchall()
    SpecData = numpy.array(results,dtype=float)
    
    cursor.close()
    cnx.close()

    if not len(SpecData):
        SpecData = numpy.zeros((2,11))
        SpecData[0,2] = vstart
        SpecData[1,2] = vend

    species_data = {
                "H":[1.008,1],
                "He":[4.0026,2],
                "Li":[6.941,3],
                "Be":[9.012,4],
                "B":[10.811,5],
                "C":[12.011,6],
                "N":[14.007,7],
                "O":[15.999,8],
                "F":[18.998,9],
                "Ne":[20.18,10],
                "Na":[22.99,11],
                "Mg":[24.305,12],
                "Al":[26.982,3],
                "Si":[28.086,3],
                "P":[30.974,3],
                "S":[32.066,3],
                "Cl":[35.453,3],
                "Ar":[39.948,3],
                "K":[39.098,19],
                "Ca":[40.078,20],
                "Sc":[44.956,3],
                "Ti":[47.88,3],
                "V":[50.942,3],
                "Cr":[51.996,3],
                "Mn":[54.938,3],
                "Fe":[55.933,3],
                "Co":[58.933,3],
                "Ni":[58.693,3],
                "Cu":[63.546,3],
                "Ga":[69.732,3],
                "Ge":[72.61,3],
                "Rb":[84.468,3],
                "Sr":[87.62,3],
                "Zr":[91.224,3],
                "Mo":[95.94,3],
                "Tc":[98.907,3],
                "Ru":[101.07,3],
                "Rh":[102.906,3],
                "Pd":[106.42,3],
                "Ag":[107.686,3],
                "In":[114.818,3],
                "Sn":[118.71,3],
                "Sb":[121.760,3],
                "Te":[127.6,3],
                "I":[126.904,3],
                "Cs":[132.905,3],
                "Ba":[137.327,3],
                "W":[183.85,3],
                "Hg":[200.59,3],
                "Bi":[208.980,3],
                "Zn":[65.39,3],
                "As":[74.922,3],
                "Br":[79.904,3],
                "Kr":[84.8,3],
                "Cd":[112.411,3],
                "Xe":[131.29,3],
                "Lu":[174.967],
                "Hf":[178.49,3],
                "Ta":[180.948,3],
                "Ir":[192.22,3],
                "Au":[196.967,3],
                "Tl":[204.383,3],
                "Pb":[207.2,3],
                "Po":[208.982,3],
                "At":[209.987,3],
                "Rn":[222.018,3],
                "Fr":[223.020,3],
                "Ra":[226.025,3],
                "Nd":[144.24,3],
                "Pm":[144.913,3],
                "Sm":[150.36,3],
                "Eu":[151.966,3],
                "Gd":[157.25,3],
                "Tb":[158.925,3],
                "Dy":[162.5,3],
                "Ho":[164.930,3],
                "Er":[167.26,3],
                "Tm":[168.934,3],
                "Yb":[173.04,3]
                }

    col = 1
    M = species_data[Species][0]
    num_ions = species_data[Species][1]
    Q_Tvect         = numpy.zeros(num_ions)
    path_file = filepath+"NIST_"+Species+"_Qelec.csv"

    Qdata       = numpy.genfromtxt(path_file, delimiter = ',')
    Tv          = Qdata[:,0]  

    for ii in range(0,num_ions):
        Q_Tv        = Qdata[:,col+ii]                    
        Q_Tvect[ii] = QT.Q_T_Function_NIST_v1(col, num_ions, T, Q_Tv,Tv)

    return M, Q_Tvect, SpecData, num_ions