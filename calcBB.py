def calcBB(T,e,vstart,vend,dv):

	import numpy



######################################################################################################################
	########################################################################################################################
	# Python does the rest
	l               = numpy.arange(vstart,vend,dv)
	v               = l
	h               = 6.626e-34         # J-s
	kb              = 1.38e-23          # J/molec-K
	c               = 2.99792458e8            # m/s
	f               = c/(v/1000000.0)         # 1/s 
	Ibb             = numpy.divide(2*h*f**3, c**2) * numpy.divide(1, numpy.exp( numpy.divide(h*f, kb*T) ) - 1 ) # Watts per Hz per steradian per m^2
	l_meter         = numpy.divide(l,1000000.0)  #microns to meters
	Ibb             = numpy.divide(2*h*c**2, l_meter**5) * numpy.divide(1, numpy.exp( numpy.divide(h*c, l_meter*kb*T) ) - 1 ) # Watts per Hz per steradian per m^2	
	#Ibb             = Ibb/c*100.0       # Watts per steradian per cm^-1 per  m^2
	#Ibb             = Ibb*(1.0/100.0)**2 # Watts per Hz per steradian per cm^2
	#Ibb             = Ibb*1000.0        # micro-Watts per steradian per cm^-1 per cm^2
	Ibb				= Ibb*e/1000000000.0
	ans = zip(v,Ibb)
	#ans = zip(v,v)
	ansWithKeys = [{'nu':x,'abs':y} for x,y in ans]
	outs = {}
	outs[0] = ansWithKeys

	return outs

