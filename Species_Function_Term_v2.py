# -*- coding: utf-8 -*-
"""
Created on Sat Jul 19 16:19:23 2014

@author: Chris Goldenstein
"""

def iter_row(cursor, conn, size=10):
    while True:
        if conn.is_connected():
            print 'connected'
        print conn

        rows = cursor.fetchmany(size)

        print 'row'
        if not rows:
            print 'breaking'
            break
        for row in rows:
            yield row

def Species_Function_Term_v2(Species,T,vstart,vend,isotope_include):
    import numpy
    import Q_T_Function_v1 as QT
    import mysql.connector
    from application import app, db
    
    #filepath    ="./static/"
    #filename    ="HITRAN_08_Partition_Function_Sums.csv"

    #filepath    ="./static/"
    #filename    ="HITRAN_08_Partition_Function_Sums.csv"
    #path_file   = filepath + filename
    path_file   = "https://s3-us-west-1.amazonaws.com/spectraplot/HITRAN_08_Partition_Function_Sums.csv"

    Qdata       = numpy.genfromtxt(path_file, delimiter = ',')

    row_Q_To    = 226
    
    M_H         = 1.008
    M_O         = 15.99
    M_C         = 12.011
    M_S         = 32.065
    M_N         = 14.007
    M_Cl        = 35.453
    M_I         = 126.90
    M_Br        = 79.904    
    M_F         = 18.998
    M_P         = 30.974
    
    if Species == "NO+":
        Species = "NOplus"
    
    #cnx = mysql.connector.connect(user='spectraplot', password='password', host='spectraplot.cq60ipps6rt2.us-west-1.rds.amazonaws.com', port='3306', database='HITRAN')
    #cursor = cnx.cursor()   
    
    if (isotope_include==1):
        query = ("SELECT * FROM `"+Species+"` WHERE WAVENUMBER BETWEEN "+str(vstart-2.5)+ " AND "+str(vend+2.5)+";")
    else: 
        query = ("SELECT * FROM `"+Species+"` WHERE WAVENUMBER BETWEEN "+str(vstart-2.5)+ " AND "+str(vend+2.5)+" AND ISOTOPENUM=1;")
    #cursor.execute(query)
    
    
    result = db.engine.execute(query)
    data = []
    for row in result:
        data.append(row)

    SpecData = numpy.array(data,dtype=float)
    
    
    
    #results = []
    #print 'before iteration'
    #for row in iter_row(cursor,cnx,10):
    #    if not row:
    #        break
    #    results.append(row)
    #results = cursor.fetchall()
    #print 'after'
    #print results
    #cursor.close()
    #cnx.close()
    #print 'done w sql'
    #SpecData = numpy.array(results,dtype=float)
    

    if not len(SpecData):
        SpecData = numpy.zeros((2,11))
        SpecData[0,2] = vstart
        SpecData[1,2] = vend

    if (Species == "H2O") or (Species == "H2O HITEMP"):

        col     = 1 
#        M       = M_H*2 + M_O
        M_vect  = numpy.array([18.010565, 20.014811, 19.01478, 19.01674, 21.020985, 20.020956])
        Tv      = Qdata[:,0]  

        num_isotopes    = 6
        
        
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)  

        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]


    elif (Species == "CO2") or (Species == "CO2 HITEMP"):
        col     = 7
#        M       = M_C + 2*M_O
        M_vect  = numpy.array([43.98983, 44.993185, 45.994076, 44.994045, 46.997431, 45.9974, 47.998322, 46.998291, 45.998262, 49.001675, 48.001646])
        Tv      = Qdata[:,0]  

        num_isotopes    = 8
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)  
        
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]

    elif Species == "O3":
        col     = 15
        #M       = 3*M_O
        M_vect  = numpy.array([47.984745, 49.988991, 49.988991, 48.98896, 48.98896])        
        Tv      = Qdata[:,0]  

        num_isotopes    = 18
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]                        

    elif Species == "N2O":
        col     = 33
#        M       = 2*M_N + M_O
        M_vect  = numpy.array([44.001062, 44.998096, 44.998096, 46.005308, 45.005278])        
        Tv      = Qdata[:,0]  

        num_isotopes    = 5
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    

        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]

    elif (Species == "CO") or (Species == "CO HITEMP"):
        col     = 38
#        M       = M_C + M_O
        M_vect  = numpy.array([27.994915, 28.99827, 29.999161, 28.99913, 31.002516, 30.002485])
        Tv      = Qdata[:,0]  

        num_isotopes    = 6
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]                        


    elif Species == "CH4":
        col     = 44
#        M       = M_C + 4*M_H
        M_vect  = numpy.array([16.0313, 17.034655, 17.037475, 18.04083 ])                
        Tv      = Qdata[:,0]  

        num_isotopes    = 3
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]                        


    elif Species == "O2":
        col     = 47
#        M       = 2*M_O
        M_vect  = numpy.array([31.98983, 33.994076, 32.994045 ])                
        Tv      = Qdata[:,0]  

        num_isotopes    = 3
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]


    elif (Species == "NO") or (Species == "NO HITEMP"):
        col     = 50
#        M       = M_N + M_O
        M_vect  = numpy.array([29.997989, 30.995023, 32.002234 ])                
        Tv      = Qdata[:,0]  

        num_isotopes    = 3
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]

        
    elif Species == "SO2":
        col     = 53
#        M       = M_S + 2*M_O
        M_vect  = numpy.array([63.961901, 65.957695 ])                
        Tv      = Qdata[:,0]  

        num_isotopes    = 2
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]
    
    elif Species == "NO2":
        col     = 55
#        M       = M_N + 2*M_O
        M_vect  = numpy.array([45.992904])                
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 1
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]

    elif Species == "NH3":
        col     = 56
#        M       = M_N + 3*M_H
        M_vect  = numpy.array([17.026549, 18.023583 ])                
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 2
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]   

    elif Species == "HNO3":
        col     = 58
#        M       = M_H + M_N + 3*M_O
        M_vect  = numpy.array([62.995644, 63.99268 ])                
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 1
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]

    elif (Species == "OH") or (Species == "OH HITEMP"):
        col     = 59
#        M       = M_O + M_H
        M_vect  = numpy.array([17.00274, 19.006986, 18.008915 ])                
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 3
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]

    elif Species == "HF":
        col     = 62
#        M       = M_H + M_F
        M_vect  = numpy.array([20.006229, 21.012404 ])                
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 1
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]

    elif Species == "HCl":
        col     = 63
#        M       = M_H + M_Cl
        M_vect  = numpy.array([35.976678, 37.973729, 36.982853, 38.979904 ])                
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 2
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]

    elif Species == "HBr":
        col     = 65
#        M       = M_H + M_Br
        M_vect  = numpy.array([79.92616, 81.924115, 80.932336, 82.930289 ])                
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 2
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]

    elif Species == "HI":
        col     = 67
#        M       = M_H + M_I
        M_vect  = numpy.array([127.912297, 128.918472 ])                
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 1
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]
    
    elif Species == "ClO":
        col     = 68
#        M       = M_Cl + M_O
        M_vect  = numpy.array([50.963768, 52.960819 ])                
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 2
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]

    elif Species == "OCS":
        col     = 70
#        M       = M_O + M_C + M_S
        M_vect  = numpy.array([59.966986, 61.96278, 60.970341, 60.966371, 61.971231 ])                
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 5
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]

    elif Species == "H2CO":
        col     = 75
#        M       = 2*M_H + M_C + M_O
        M_vect  = numpy.array([30.010565, 31.01392, 32.014811 ])                
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 3
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]

    elif Species == "HOCl":
        col     = 78
#        M       = M_H + M_O + M_Cl
        M_vect  = numpy.array([51.971593, 53.968644 ])                
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 2
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]

    elif Species == "N2":
        col     = 80
#        M       = 2*M_N
        M_vect  = numpy.array([28.006148, 29.003182 ])                
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 1
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]
    
    elif Species == "HCN":
        col     = 81
#        M       = M_H + M_C + M_N
        M_vect  = numpy.array([27.010899, 28.014254, 28.007933 ])        
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 3
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]

    elif Species == "CH3Cl":
        col     = 84
#        M       = M_C + 3*M_H + M_Cl 
        M_vect  = numpy.array([49.992328, 51.989379 ])                
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 2
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]

    elif Species == "H2O2":
        col     = 88
#        M       = 2*M_H + 2*M_O
        M_vect  = numpy.array([34.00548 ])                
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 1
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]


    elif Species == "C2H2":
        col     = 87
#        M       = 2*M_C + 2*M_H
        M_vect  = numpy.array([26.01565, 27.019005, 27.021825 ])                
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 2
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]

    elif Species == "C2H6":
        col     = 89
#        M       = 2*M_C + 6*M_H
        M_vect  = numpy.array([30.04695, 31.050305])                
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 1
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]

    elif Species == "PH3":
        col     = 90
#        M       = M_P + 3*M_H
        M_vect  = numpy.array([33.997238])                
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 1
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]

    elif Species == "COF2":
        col     = 91
#        M       = M_C + M_O + 2*M_F
        M_vect  = numpy.array([65.991722, 66.995083 ])                
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 1
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]

    elif Species == "SF6":
        col     = 92
#        M       = M_S + 6*M_F
        M_vect  = numpy.array([145.962492])                
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 1
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]

    elif Species == "H2S":
        col     = 93
#        M       = 2*M_H + M_S
        M_vect  = numpy.array([33.987721, 35.983515, 34.987105 ])                
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 3
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]    

    elif Species == "HCOOH":
        col     = 96
#        M       = M_H + M_C + 2*M_O + M_H
        M_vect  = numpy.array([46.00548])                
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 1
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]

    elif Species == "HO2":
        col     = 97
#        M       = M_H + 2*M_O
        M_vect  = numpy.array([32.997655])                
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 1
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]

    elif Species == "O":
        col     = 98
#        M       = M_O
        M_vect  = numpy.array([15.994915])                
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 1
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]

    elif Species == "ClONO2":
        col     = 99
#        M       = M_Cl + M_O + M_N + 2*M_O
        M_vect  = numpy.array([96.956672, 98.953723 ])                
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 2
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]

    elif Species == "NOplus":
        col     = 101
#        M       = M_N + M_O
        M_vect  = numpy.array([29.997989])                
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 1
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]

    elif Species == "HOBr":
        col     = 102
#        M       = M_H + M_O + M_Br
        M_vect  = numpy.array([95.921076, 97.919027 ])                
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 2
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]

    elif Species == "C2H4":
        col     = 104
#        M       = 2*M_C + 4*M_H
        M_vect  = numpy.array([28.0313, 29.034655 ])        
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 2
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]                        
        
    elif Species == "CH3OH":
        col     = 106
#        M       = M_C + 3*M_H + M_O + M_H
        M_vect  = numpy.array([32.026215])        
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 1
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]

    elif Species == "CH3CN":
        col     = 107
#        M       = M_C + 3*M_H + M_C + M_N
        M_vect  = numpy.array([41.026549])                
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 1
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]

    elif Species == "CF4":
        col     = 108
#        M       = M_C + 4*M_F
        M_vect  = numpy.array([87.993616])                
        Tv      = Qdata[:,0]  
 
        num_isotopes    = 1
        Q_Tvect         = numpy.zeros(num_isotopes)
        Q_Tovect        = numpy.zeros(num_isotopes)    
        for ii in range(0,num_isotopes):
            Q_Tv        = Qdata[:,col+ii]                    
            Q_Tvect[ii] = QT.Q_T_Function_v1(col,row_Q_To, num_isotopes, T,Q_Tv,Tv)
            Q_Tovect[ii]= Qdata[row_Q_To,col+ii]
    

    return M_vect, Q_Tovect, Q_Tvect, SpecData, num_isotopes
