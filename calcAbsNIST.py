def calcAbsNIST(T,P,L,vstart,vend,dv,Species_Vector,Xabs_Vector,gammaTo,n,Telec):

	import Absorbance_Code_NIST_Function_v1 as NAF
	import numpy



	######################################################################################################################
	# User inputs
	if P<=0.5:
		wing_eval=2
	elif P<5:
		wing_eval=P*10.0
	else:
		wing_eval=50
	
	########################################################################################################################
	# Python does the rest
	v               = numpy.arange(vstart,vend,dv)
	num_species     = len(Species_Vector)
	length_v        = len(v)
	output			= numpy.zeros((length_v,num_species))
	EinAFlag		= {}
	inputs          = Species_Vector, T, P, Xabs_Vector, L, dv, vstart, vend, wing_eval, gammaTo, n, Telec
	outs = {}

	for ii in range(0,num_species):	
		inputs          = Species_Vector, T, P, Xabs_Vector, L, dv, vstart, vend, wing_eval, gammaTo, n, Telec
		output[:,ii],EinsteinAFlag   = NAF.Absorbance_Code_NIST_Function_v1([inputs,ii])  
		ans = zip(v,output[:,ii])
		ansWithKeys = [{'nu':x, 'abs':y} for x,y in ans]
		outs[ii] 		= ansWithKeys
		EinAFlag[ii]=EinsteinAFlag
		
	return outs,EinAFlag

