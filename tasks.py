from calculators import calcAbs as calcAbs
from calculators import calcAbsNIST as calcAbsNIST 
from calculators import calcEmission as calcEmission 
from calculators import calcEmissionNIST as calcEmissionNIST 
from calculators import calcSurvey as calcSurvey
from calculators import calcSurveyNIST as calcSurveyNIST
from calculators import calcBB as calcBB
from flask import request, jsonify
import simplejson
from application import app, db

def flipv(vstart,vend):
	if vend<vstart:
		vendHold = vstart
		vstart   = vend
		vend     = vendHold
	return vstart,vend

def downSampleDeltav(vstart,vend,deltav):
	numPts = (vend-vstart)/deltav
	if numPts > 30000:
		deltav = (vend-vstart)/30000
	return deltav

def packValuesForQuery(parameters,calculator):
	
	molefracs = parameters['molefracs']
	molefracsString = [str(x) for x in molefracs]
	
	valuesForQuery = {
            'IP': parameters['IPaddress'],
            'Temperature': parameters['T'],
            'Pressure': parameters['P'],
            'PathLength': parameters['L'],
            'vStart': parameters['vstart'],
            'vEnd': parameters['vend'],
            'vStep': parameters['deltav'],
            'species': ', '.join(parameters['species']),
            'x': ', '.join(molefracsString),
            'cook': str(parameters['cook']),
            'sim': calculator,
            'db': parameters['simType'],
            'Telec': parameters['Telec'],
            'Scutoff': parameters['Scutoff']}

	return valuesForQuery



def calculate_absorbtion(parameters):
		
	T = parameters['T']
	P = parameters['P']
	L = parameters['L']
	vstart = parameters['vstart']
	vend = parameters['vend']
	deltav = parameters['deltav']
	species = parameters['species']
	molefracs = parameters['molefracs']
	gammaTo = parameters['gammaTo']
	n = parameters['n']
	Telec = parameters['Telec']
	firstPlot = parameters['firstPlot']
	simType = parameters['simType']

	vstart,vend = flipv(vstart,vend)
	deltav = downSampleDeltav(vstart,vend,deltav)

	EinAFlag = {}
    
	if firstPlot == 0:
		file = open("./static/initPlot.txt", "r")
		data = simplejson.load(file)
		file.close()
		laserResults, detectorResults, opticsResults, filtersResults, fibersResults, mirrorsResults = 0, 0, 0, 0 ,0 ,0 
		EinAFlag[0] = 0
	else:    
		valuesForQuery = packValuesForQuery(parameters,'Absorption')
		logSimulation(valuesForQuery)

		if simType=='hitran':           
			data = calcAbs.calcAbs(T,P,L,vstart,vend,deltav,species,molefracs)
			EinAFlag[0] = 0
		elif simType=='nist':
			data,EinAFlag = calcAbsNIST.calcAbsNIST(T,P,L,vstart,vend,deltav,species,molefracs,gammaTo,n,Telec)

		laserResults, detectorResults, opticsResults, filtersResults, fibersResults, mirrorsResults = returnInventories(vstart,vend)

	return jsonify(output=data,EinsteinAFlag=EinAFlag,deltavcalc=deltav,lasers=laserResults,detectors=detectorResults,optics=opticsResults,filters=filtersResults,fibers=fibersResults,mirrors=mirrorsResults)

def calculate_emission(parameters):
	T = parameters['T']
	P = parameters['P']
	L = parameters['L']
	vstart = parameters['vstart']
	vend = parameters['vend']
	deltav = parameters['deltav']
	species = parameters['species']
	molefracs = parameters['molefracs']
	gammaTo = parameters['gammaTo']
	n = parameters['n']
	Telec = parameters['Telec']
	firstPlot = parameters['firstPlot']
	simType = parameters['simType']

	vstart,vend = flipv(vstart,vend)
	deltav = downSampleDeltav(vstart,vend,deltav)

	EinAFlag = {}

	if firstPlot == 0:
		file = open("./static/initEmisPlot.txt", "r")
		data = simplejson.load(file)
		file.close()
		laserResults, detectorResults, opticsResults, filtersResults, fibersResults, mirrorsResults = 0, 0, 0, 0 ,0 ,0 
		EinAFlag[0] = 0
	else:    
		valuesForQuery = packValuesForQuery(parameters,'Emission') 
		logSimulation(valuesForQuery)
		if simType=='hitran':           
			data = calcEmission.calcEmission(T,P,L,vstart,vend,deltav,species,molefracs)
			EinAFlag[0] = 0
		elif simType=='nist':
			data,EinAFlag = calcEmissionNIST.calcEmissionNIST(T,P,L,vstart,vend,deltav,species,molefracs,gammaTo,n,Telec)           
         
		laserResults, detectorResults, opticsResults, filtersResults, fibersResults, mirrorsResults = returnInventories(vstart,vend)
	return jsonify(output=data,EinsteinAFlag=EinAFlag,deltavcalc=deltav,lasers=laserResults,detectors=detectorResults,optics=opticsResults,filters=filtersResults,fibers=fibersResults,mirrors=mirrorsResults)


def calculate_survey(parameters):
	T = parameters['T']
	P = parameters['P']
	L = parameters['L']
	isotope_include = parameters['isotope_include']
	vstart = parameters['vstart']
	vend = parameters['vend']
	deltav = parameters['deltav']
	species = parameters['species']
	molefracs = parameters['molefracs']
	
	Telec = parameters['Telec']
	firstPlot = parameters['firstPlot']
	simType = parameters['simType']

	vstart,vend = flipv(vstart,vend)
	

	if firstPlot == 0:
		file = open("./static/initSurveyPlot.txt", "r")
		data = simplejson.load(file)
		laserResults, detectorResults, opticsResults, filtersResults, fibersResults, mirrorsResults = 0, 0, 0, 0 ,0 ,0 
		file.close()
	else:    
		valuesForQuery = packValuesForQuery(parameters,'Survey') 
		logSimulation(valuesForQuery)
		if simType=='hitran':           
			data = calcSurvey.calcSurvey(T,P,L,vstart,vend,deltav,species,molefracs,isotope_include)
		elif simType=='nist':
			data = calcSurveyNIST.calcSurveyNIST(Telec,vstart,vend,species,molefracs)           

		laserResults, detectorResults, opticsResults, filtersResults, fibersResults, mirrorsResults = returnInventories(vstart,vend)
	return jsonify(output=data,deltavcalc=deltav,lasers=laserResults,detectors=detectorResults,optics=opticsResults,filters=filtersResults,fibers=fibersResults,mirrors=mirrorsResults)

def calculate_blackbody(parameters):
	T = parameters['T']
	e = parameters['e']
	
	vstart = parameters['vstart']
	vend = parameters['vend']
	deltav = .01
	vstart,vend = flipv(vstart,vend)
	lstart = 10000/vend
	lend   = 10000/vstart

	firstPlot = parameters['firstPlot']
	simType = 'BlackBody'
    
	if firstPlot == 0:
		file = open("./static/initPlotBB.txt", "r")
		data = simplejson.load(file)
		laserResults, detectorResults, opticsResults, filtersResults, fibersResults, mirrorsResults = 0, 0, 0, 0 ,0 ,0 
		file.close()
	else:    
		valuesForQuery = packValuesForQuery(parameters,'BlackBody')  
		logSimulation(valuesForQuery)           
		data  = calcBB.calcBB(T,e,lstart,lend,deltav)
		laserResults, detectorResults, opticsResults, filtersResults, fibersResults, mirrorsResults = returnInventories(vstart,vend)
		#jsondata = simplejson.dumps(data)
		#f = open("/usr/share/nginx/spectraplot/app/static/initPlotBB.txt","w")
		#f.write(jsondata)
		#f.close()
	return jsonify(output=data,deltavcalc=deltav,lasers=laserResults,detectors=detectorResults,optics=opticsResults,filters=filtersResults,fibers=fibersResults,mirrors=mirrorsResults)

def calculate_hardware(parameters):
	
	vstart = parameters['vstart']
	vend = parameters['vend']
	vstart,vend = flipv(vstart,vend)
	valuesForQuery = packValuesForQuery(parameters,'Search')  

	logSimulation(valuesForQuery)
	laserResults, detectorResults, opticsResults, filtersResults, fibersResults, mirrorsResults = returnInventories(vstart,vend)
	return jsonify(lasers=laserResults,detectors=detectorResults,optics=opticsResults,filters=filtersResults,fibers=fibersResults,mirrors=mirrorsResults)


def logSimulation(valuesForQuery):
    
    query = ("INSERT INTO PrelimLogs (IPaddress, Temperature, Pressure, PathLength, "
    	"vStart, vEnd, vStep, species, x, GAcookie, simType, db, Telec, Scutoff)"
    	"VALUES (%(IP)s, %(Temperature)s, %(Pressure)s, %(PathLength)s, %(vStart)s, %(vEnd)s, "
    	"%(vStep)s, %(species)s, %(x)s, %(cook)s, %(sim)s, %(db)s, %(Telec)s, %(Scutoff)s)")
    
    result = db.get_engine(app,bind='simlogs').execute(query,valuesForQuery)
    #cursor.execute(query,valuesForQuery)
    #cnx.commit()
    #cursor.close()
    #               

###################################
#define catalogue functions
###################################

def returnInventories(vstart,vend): 

	query = ("SELECT * FROM `Lasers` WHERE nuStartCm BETWEEN "+str(vstart)+" AND "+str(vend)+" OR nuEndCm BETWEEN "+str(vstart)+" AND "+str(vend)+" OR "+str(vstart)+" BETWEEN nuStartCm AND nuEndCm OR "+str(vend)+" BETWEEN nuStartCm and nuEndCm;")
	result = db.get_engine(app,bind='hardware').execute(query)
	laserResults = []
	for row in result:
		laserResults.append(row[0:11])  #omit ID - for some reason, must include index of ID, but this does not include ID in appendage
         
	query = ("SELECT * FROM `Detectors` WHERE nuStartCm BETWEEN "+str(vstart)+" AND "+str(vend)+" OR nuEndCm BETWEEN "+str(vstart)+" AND "+str(vend)+" OR "+str(vstart)+" BETWEEN nuStartCm AND nuEndCm OR "+str(vend)+" BETWEEN nuStartCm and nuEndCm;")
	result = db.get_engine(app,bind='hardware').execute(query)
	detectorResults = []
	for row in result:
		detectorResults.append(row[0:10])  #omit ID
	
	query = ("SELECT * FROM `Optics` WHERE nuStartCm BETWEEN "+str(vstart)+" AND "+str(vend)+" OR nuEndCm BETWEEN "+str(vstart)+" AND "+str(vend)+" OR "+str(vstart)+" BETWEEN nuStartCm AND nuEndCm OR "+str(vend)+" BETWEEN nuStartCm and nuEndCm;")
	result = db.get_engine(app,bind='hardware').execute(query)
	opticsResults = []
	for row in result:
		opticsResults.append(row[0:12])  #omit ID

	query = ("SELECT * FROM `Filters` WHERE nuStartCm BETWEEN "+str(vstart)+" AND "+str(vend)+" OR nuEndCm BETWEEN "+str(vstart)+" AND "+str(vend)+" OR "+str(vstart)+" BETWEEN nuStartCm AND nuEndCm OR "+str(vend)+" BETWEEN nuStartCm and nuEndCm;")
	result = db.get_engine(app,bind='hardware').execute(query)
	filtersResults = []
	for row in result:
		filtersResults.append(row[0:10])  #omit ID

	query = ("SELECT * FROM `Fibers` WHERE nuStartCm BETWEEN "+str(vstart)+" AND "+str(vend)+" OR nuEndCm BETWEEN "+str(vstart)+" AND "+str(vend)+" OR "+str(vstart)+" BETWEEN nuStartCm AND nuEndCm OR "+str(vend)+" BETWEEN nuStartCm and nuEndCm;")
	result = db.get_engine(app,bind='hardware').execute(query)
	fibersResults = []
	for row in result:
		fibersResults.append(row[0:11])  #omit ID
	     
	query = ("SELECT * FROM `Mirrors` WHERE nuStartCm BETWEEN "+str(vstart)+" AND "+str(vend)+" OR nuEndCm BETWEEN "+str(vstart)+" AND "+str(vend)+" OR "+str(vstart)+" BETWEEN nuStartCm AND nuEndCm OR "+str(vend)+" BETWEEN nuStartCm and nuEndCm;")
	result = db.get_engine(app,bind='hardware').execute(query)
	mirrorsResults = []
	for row in result:
		mirrorsResults.append(row[0:10])  #omit ID
		
	return laserResults, detectorResults, opticsResults, filtersResults, fibersResults, mirrorsResults   