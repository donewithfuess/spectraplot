# -*- coding: utf-8 -*-
"""
Created on Sat Jul 19 15:44:48 2014

@author: Chris Goldenstein
"""
#Temperature dependent linestrength [cm-2/atm]

def Linestrength_Function_v1(vo,E,S_To,Q_To,Q_T,T):
    import math
    
    


    #Constants
    h               = 6.626068*10**(-34)                # Planck's Constant in m^2*kg/s
    k               = 1.3806504*10**(-23)                # Boltzmann's Constant (J/K)
    c               = 2.99792458*10**10              # Speed of Light (cm/s)
    To              = 296.0
 
    Boltz           = ( 1-math.exp(-h*c*vo/(k*T)))/(1-math.exp(-h*c*vo/(k*To))) 
    S_hitran_units  =  S_To*Q_To/Q_T*Boltz*math.exp(-((h*c*E)/k)*(1/T - 1/To)) #Output in HITRAN units (number-density basis)
    S               = (7.34*10**21)*S_hitran_units/T
        
    return S