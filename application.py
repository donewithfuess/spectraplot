from flask import Flask, jsonify, render_template, request, Response, redirect, url_for, send_from_directory, send_file
from werkzeug import secure_filename
from werkzeug.contrib.fixers import ProxyFix
import os
import json
import pandas as pd
from functools import wraps
from calcAbs import calcAbs
from calcAbsNIST import calcAbsNIST 
from calcEmission import calcEmission 
from calcEmissionNIST import calcEmissionNIST 
from calcSurvey import calcSurvey
from calcSurveyNIST import calcSurveyNIST
from calcBB import calcBB
from datetime import datetime
import numpy as np
import StringIO
import zipfile
import simplejson
import mysql.connector
from flask.ext.mail import Message, Mail
from flask.ext.sqlalchemy import SQLAlchemy


mail = Mail()
application = Flask(__name__)
app = application

app.wsgi_app = ProxyFix(app.wsgi_app)
app.config['PROPAGATE_EXCEPTIONS'] = True


###################################
#define mysql database
###################################

#mysql://username:password@server/db
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://spectraplot:password@spectraplot.cq60ipps6rt2.us-west-1.rds.amazonaws.com/HITRAN'
db = SQLAlchemy(app)


###################################
#define mailer functionality
###################################
app.config["MAIL_SERVER"] = "smtp.zoho.com"
app.config["MAIL_PORT"] = 465
app.config["MAIL_USE_SSL"] = True
app.config["MAIL_USERNAME"] = 'info@spectraplot.com'
app.config["MAIL_PASSWORD"] = 'spectroscopyisfun'
 
mail.init_app(app)




###################################
#define uploader functionality
###################################
# This is the path to the upload directory
app.config['UPLOAD_FOLDER'] = './vendors/'
# These are the extension that we are accepting to be uploaded
app.config['ALLOWED_EXTENSIONS'] = set(['csv', 'xls','xlsx'])

# For a given file, return whether it's an allowed type or not
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']

###################################
#define the calculation call for absorption
###################################
@app.route('/_calcAbs')
def calculate_absorbtion():
    T = request.args.get('T', 0, type=float)
    P = request.args.get('P', 0, type=float)
    L = request.args.get('L', 0, type=float)
    vstart = request.args.get('vstart',0,type=float)
    vend = request.args.get('vend',0,type=float)
    deltav = request.args.get('deltav',0,type=float)
    firstPlot = request.args.get('firstPlotFlag', 0, type=float)
    simType = request.args.get('simType')
    gammaTo = request.args.get('gammaTo', 0, type=float)
    n = request.args.get('n', 0, type=float)
    Telec = request.args.get('Telec', 0, type=float)
    
    if vend<vstart:
        vendHold = vstart
        vstart   = vend
        vend     = vendHold

    numPts = (vend-vstart)/deltav
    if numPts > 10000:
        deltav = (vend-vstart)/10000
    
    #if not request.headers.getlist("X-Forwarded-For"):
    #    ip = request.remote_addr
    #else:
    #    ip = request.headers.getlist("X-Forwarded-For")[0]
    
    #print ip
    #print request.remote_addr

    species = request.args.getlist('species[]')
    species =  [str(x) for x in species]
    
    molefracs = request.args.getlist('molefracs[]',type=float)
    molefracsString = [str(x) for x in molefracs]

    cook = request.args.get('cook')

    EinAFlag = {}
    
    if firstPlot == 0:
         file = open("./static/initPlot.txt", "r")
         #file = open("/Users/vic/Desktop/repos/test.spectraplot.com/app/static/initPlot.txt", "r")
         data = simplejson.load(file)
         file.close()
         laserResults    = 0
         detectorResults = 0
         opticsResults   = 0         
         filtersResults  = 0
         fibersResults   = 0
         mirrorsResults  = 0
         EinAFlag[0] = 0
    else:    
         valuesForQuery = {
         'IP': request.remote_addr,
         'Temperature': T,
         'Pressure': P,
         'PathLength': L,
         'vStart': vstart,
         'vEnd': vend,
         'vStep': deltav,
         'species': ', '.join(species),
         'x': ', '.join(molefracsString),
         'cook': str(cook),
         'sim': 'Absorption',
         'db': simType,
         'Telec': Telec,
         'Scutoff': 0}        

         logSimulation(valuesForQuery)
         if simType=='hitran':           
              data = calcAbs(T,P,L,vstart,vend,deltav,species,molefracs)
              EinAFlag[0] = 0
         elif simType=='nist':
              data,EinAFlag = calcAbsNIST(T,P,L,vstart,vend,deltav,species,molefracs,gammaTo,n,Telec)

         laserResults, detectorResults, opticsResults, filtersResults, fibersResults, mirrorsResults = returnInventories(vstart,vend)
    #jsondata = simplejson.dumps(data)
    #f = open("/usr/share/nginx/spectraplot/app/static/initPlot.txt","w")
    #f.write(jsondata)
    #f.close()
    return jsonify(output=data,EinsteinAFlag=EinAFlag,deltavcalc=deltav,lasers=laserResults,detectors=detectorResults,optics=opticsResults,filters=filtersResults,fibers=fibersResults,mirrors=mirrorsResults)

###################################
#define the calculation call for emission
###################################

@app.route('/_calcEmission')
def calculate_emission():
    T = request.args.get('T', 0, type=float)
    P = request.args.get('P', 0, type=float)
    L = request.args.get('L', 0, type=float)
    vstart = request.args.get('vstart',0,type=float)
    vend = request.args.get('vend',0,type=float)
    deltav = request.args.get('deltav',0,type=float)
    firstPlot = request.args.get('firstPlotFlag', 0, type=float)
    simType = request.args.get('simType')
    gammaTo = request.args.get('gammaTo', 0, type=float)
    n = request.args.get('n', 0, type=float)
    Telec = request.args.get('Telec', 0, type=float)

    if vend<vstart:
        vendHold = vstart
        vstart   = vend
        vend     = vendHold

    numPts = (vend-vstart)/deltav
    if numPts > 10000:
        deltav = (vend-vstart)/10000
    
    species = request.args.getlist('species[]')
    species =  [str(x) for x in species]
    
    molefracs = request.args.getlist('molefracs[]',type=float)
    molefracsString = [str(x) for x in molefracs]

    cook = request.args.get('cook')

    EinAFlag = {}
    
    if firstPlot == 0:
         file = open("./static/initEmisPlot.txt", "r")
         data = simplejson.load(file)
         file.close()
         laserResults    = 0
         detectorResults = 0
         opticsResults   = 0         
         filtersResults  = 0
         fibersResults   = 0
         mirrorsResults  = 0
         EinAFlag[0] = 0
    else:    
         valuesForQuery = {
         'IP': request.remote_addr,
         'Temperature': T,
         'Pressure': P,
         'PathLength': L,
         'vStart': vstart,
         'vEnd': vend,
         'vStep': deltav,
         'species': ', '.join(species),
         'x': ', '.join(molefracsString),
         'cook': str(cook),
         'sim': 'Emission',
         'db': simType,
         'Telec': Telec,
         'Scutoff': 0}        
 
         logSimulation(valuesForQuery)
         if simType=='hitran':           
              data = calcEmission(T,P,L,vstart,vend,deltav,species,molefracs)
              EinAFlag[0] = 0
         elif simType=='nist':
              data,EinAFlag = calcEmissionNIST(T,P,L,vstart,vend,deltav,species,molefracs,gammaTo,n,Telec)           
         
         laserResults, detectorResults, opticsResults, filtersResults, fibersResults, mirrorsResults = returnInventories(vstart,vend)
    return jsonify(output=data,EinsteinAFlag=EinAFlag,deltavcalc=deltav,lasers=laserResults,detectors=detectorResults,optics=opticsResults,filters=filtersResults,fibers=fibersResults,mirrors=mirrorsResults)

###################################
#define the calculation call for line survey
###################################

@app.route('/_calcSurvey')
def calculate_survey():

    T = request.args.get('T', 0, type=float)
    P = request.args.get('P', 0, type=float)
    isotope_include = request.args.get('isotope_include',0,type=float)
    L = 100
    vstart = request.args.get('vstart',0,type=float)
    vend = request.args.get('vend',0,type=float)
    deltav = 1
    Scutoff = request.args.get('scut',0,type=float)
    firstPlot = request.args.get('firstPlotFlag', 0, type=float)
    simType = request.args.get('simType')
    Telec = request.args.get('Telec',0,type=float)

    if vend<vstart:
        vendHold = vstart
        vstart   = vend
        vend     = vendHold
    
    species = request.args.getlist('species[]')
    species =  [str(x) for x in species]
    
    molefracs = request.args.getlist('molefracs[]',type=float)
    molefracsString = [str(x) for x in molefracs]
    

    cook = request.args.get('cook')
    
    if firstPlot == 0:
         file = open("./static/initSurveyPlot.txt", "r")
         data = simplejson.load(file)
         laserResults    = 0
         detectorResults = 0
         opticsResults   = 0         
         filtersResults  = 0
         fibersResults   = 0
         mirrorsResults  = 0
         file.close()
    else:    
         valuesForQuery = {
         'IP': request.remote_addr,
         'Temperature': T,
         'Pressure': P,
         'PathLength': 0,
         'vStart': vstart,
         'vEnd': vend,
         'vStep': 0,
         'species': ', '.join(species),
         'x': ', '.join(molefracsString),
         'cook': str(cook),
         'sim': 'Survey',
         'db': simType,
         'Telec': Telec,
         'Scutoff': Scutoff}        
         
         logSimulation(valuesForQuery)
         if simType=='hitran':           
            data = calcSurvey(T,P,L,vstart,vend,deltav,species,molefracs,isotope_include)
         elif simType=='nist':
            data = calcSurveyNIST(Telec,vstart,vend,species,molefracs)           

         laserResults, detectorResults, opticsResults, filtersResults, fibersResults, mirrorsResults = returnInventories(vstart,vend)
         print 'sending data back'
    return jsonify(output=data,deltavcalc=deltav,lasers=laserResults,detectors=detectorResults,optics=opticsResults,filters=filtersResults,fibers=fibersResults,mirrors=mirrorsResults)

@app.route('/_calcBB')
def calculate_blackbody():
    T = request.args.get('T', 0, type=float)
    e = request.args.get('e', 0, type=float)
    vstart = request.args.get('vstart',0,type=float)
    vend = request.args.get('vend',0,type=float)
    lstart = 10000/vstart
    lend   = 10000/vend
    firstPlot = request.args.get('firstPlotFlag', 0, type=float)
    cook = request.args.get('cook')    
    if vend<vstart:
        vendHold = vstart
        vstart   = vend
        vend     = vendHold
    deltav = .01

    
    if firstPlot == 0:
         file = open("./static/initPlotBB.txt", "r")
         data = simplejson.load(file)
         file.close()
         laserResults    = 0
         detectorResults = 0
         opticsResults   = 0         
         filtersResults  = 0
         fibersResults   = 0
         mirrorsResults  = 0
    else:    
         valuesForQuery = {
         'IP': request.remote_addr,
         'Temperature': T,
         'Pressure': 0,
         'PathLength': e,
         'vStart': vstart,
         'vEnd': vend,
         'vStep': deltav,
         'species': 'BlackBody',
         'x': 'BlackBody',
         'cook': str(cook),
         'sim': 'BlackBody',
         'db': 'BlackBody',
         'Telec': T,
         'Scutoff': 0}        
 
         logSimulation(valuesForQuery)           
         #data = calcEmission(T,1,1,vstart,vend,deltav,['CO'],[.01])
         data  = calcBB(T,e,lstart,lend,deltav)
         laserResults, detectorResults, opticsResults, filtersResults, fibersResults, mirrorsResults = returnInventories(vstart,vend)
         #jsondata = simplejson.dumps(data)
         #f = open("/usr/share/nginx/spectraplot/app/static/initPlotBB.txt","w")
         #f.write(jsondata)
         #f.close()

    return jsonify(output=data,deltavcalc=deltav,lasers=laserResults,detectors=detectorResults,optics=opticsResults,filters=filtersResults,fibers=fibersResults,mirrors=mirrorsResults)

@app.route('/_calcHW')
def calculate_hardware():
    vstart = request.args.get('vstart',0,type=float)
    vend = request.args.get('vend',0,type=float)
    cook = request.args.get('cook')    
    
    if vend<vstart:
        vendHold = vstart
        vstart   = vend
        vend     = vendHold

    valuesForQuery = {
    'IP': request.remote_addr,
    'Temperature': 0,
    'Pressure': 0,
    'PathLength': 0,
    'vStart': vstart,
    'vEnd': vend,
    'vStep': 0,
    'species': 'Search',
    'x': '0',
    'cook': str(cook),
    'sim': 'Search',
    'db': 'Search',
    'Telec': 0,
    'Scutoff': 0}   

    logSimulation(valuesForQuery)
    laserResults, detectorResults, opticsResults, filtersResults, fibersResults, mirrorsResults = returnInventories(vstart,vend)
    return jsonify(lasers=laserResults,detectors=detectorResults,optics=opticsResults,filters=filtersResults,fibers=fibersResults,mirrors=mirrorsResults)

@app.route('/_saveCSV',methods=['GET','POST'])
def save_CSV():

    data = request.form['data']
    dataJSON = json.loads(data)
    
    conditions = request.form['conditions']
    #line strings for each simulation must be split by commas
    conditions = conditions.split(',')
    
    
    files = []
    
    for i in range(0,len(conditions)):
        
        d = pd.DataFrame(dataJSON["line"+str(i)])
        mem_file = StringIO.StringIO()
        d.to_csv(mem_file, encoding='utf-8', index=False,columns=['nu','abs'],header=['nu',str(conditions[i])])
        mem_file.seek(0)
        files.append(mem_file)
    
    
    zipped_file = StringIO.StringIO()
    with zipfile.ZipFile(zipped_file, 'w') as zip:
        for i, file in enumerate(files):
            file.seek(0)
            flnm = conditions[i].replace('/',',')
            
            zip.writestr(str(flnm)+",simNum{}.csv".format(i), file.read())

    zipped_file.seek(0)

    return send_file(zipped_file,attachment_filename="SpectraPlotSimulations.zip",as_attachment=True)
    #return jsonify(result={"status": 200})

###################################
#define catalogue functions
###################################
   
def returnInventories(vstart,vend): 
         cnx = mysql.connector.connect(user='spectraplot', password='password', host='spectraplot.cq60ipps6rt2.us-west-1.rds.amazonaws.com', port='3306', database='HARDWARE')
         cursor = cnx.cursor()   
         query = ("SELECT * FROM `Lasers` WHERE nuStartCm BETWEEN "+str(vstart)+" AND "+str(vend)+" OR nuEndCm BETWEEN "+str(vstart)+" AND "+str(vend)+" OR "+str(vstart)+" BETWEEN nuStartCm AND nuEndCm OR "+str(vend)+" BETWEEN nuStartCm and nuEndCm;")
         cursor.execute(query)
         laserResults = cursor.fetchall()
         
         query = ("SELECT * FROM `Detectors` WHERE nuStartCm BETWEEN "+str(vstart)+" AND "+str(vend)+" OR nuEndCm BETWEEN "+str(vstart)+" AND "+str(vend)+" OR "+str(vstart)+" BETWEEN nuStartCm AND nuEndCm OR "+str(vend)+" BETWEEN nuStartCm and nuEndCm;")
         cursor.execute(query)
         detectorResults = cursor.fetchall()
         
         query = ("SELECT * FROM `Optics` WHERE nuStartCm BETWEEN "+str(vstart)+" AND "+str(vend)+" OR nuEndCm BETWEEN "+str(vstart)+" AND "+str(vend)+" OR "+str(vstart)+" BETWEEN nuStartCm AND nuEndCm OR "+str(vend)+" BETWEEN nuStartCm and nuEndCm;")
         cursor.execute(query)
         opticsResults = cursor.fetchall()
         
         query = ("SELECT * FROM `Filters` WHERE nuStartCm BETWEEN "+str(vstart)+" AND "+str(vend)+" OR nuEndCm BETWEEN "+str(vstart)+" AND "+str(vend)+" OR "+str(vstart)+" BETWEEN nuStartCm AND nuEndCm OR "+str(vend)+" BETWEEN nuStartCm and nuEndCm;")
         cursor.execute(query)
         filtersResults = cursor.fetchall()  
         
         query = ("SELECT * FROM `Fibers` WHERE nuStartCm BETWEEN "+str(vstart)+" AND "+str(vend)+" OR nuEndCm BETWEEN "+str(vstart)+" AND "+str(vend)+" OR "+str(vstart)+" BETWEEN nuStartCm AND nuEndCm OR "+str(vend)+" BETWEEN nuStartCm and nuEndCm;")
         cursor.execute(query)
         fibersResults = cursor.fetchall()
         
         query = ("SELECT * FROM `Mirrors` WHERE nuStartCm BETWEEN "+str(vstart)+" AND "+str(vend)+" OR nuEndCm BETWEEN "+str(vstart)+" AND "+str(vend)+" OR "+str(vstart)+" BETWEEN nuStartCm AND nuEndCm OR "+str(vend)+" BETWEEN nuStartCm and nuEndCm;")
         cursor.execute(query)
         mirrorsResults = cursor.fetchall()    

         cursor.close()
         cnx.close()
         return laserResults, detectorResults, opticsResults, filtersResults, fibersResults, mirrorsResults   

def logSimulation(valuesForQuery):
    cnx = mysql.connector.connect(user='spectraplot', password='password', host='spectraplot.cq60ipps6rt2.us-west-1.rds.amazonaws.com', port='3306', database='SimulationLog')
    cursor = cnx.cursor()   
    query = ("INSERT INTO PrelimLogs "
            "(IPaddress, Temperature, Pressure, PathLength, vStart, vEnd, vStep, species, x, GAcookie, simType, db, Telec, Scutoff) "
            "VALUES (%(IP)s, %(Temperature)s, %(Pressure)s, %(PathLength)s, %(vStart)s, %(vEnd)s, %(vStep)s, %(species)s, %(x)s, %(cook)s, %(sim)s, %(db)s, %(Telec)s, %(Scutoff)s)")
    cursor.execute(query,valuesForQuery)
    cnx.commit()
    cursor.close()
    cnx.close()                  


###################################
#define the authentication routine
###################################

def check_auth(username,password):
    return username == 'preview' and password == 'secret'

def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response(
    'Could not verify your access level for that URL.\n'
    'You have to login with proper credentials', 401,
    {'WWW-Authenticate': 'Basic realm="Login Required"'})

def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)
    return decorated


###################################
#define all the routes
###################################

@app.route("/")
def index():
    return render_template('index.html')

@app.route("/absorption")
def absorption():
    return render_template('absorption.html')

@app.route("/emission")
def emission():
    return render_template('emission.html')

@app.route("/hardware")
def hardware():
    return render_template('hardware.html')    
    
@app.route("/survey")
def survey():
    return render_template('survey.html')    
    
@app.route("/blackbody")
def blackbody():
    return render_template('blackbody.html')        

@app.route('/secret-page')
@requires_auth
def secret_page():
    return render_template('index_secret.html')
          
@app.route('/vendor')
def vendor():
    return render_template('vendor.html')                                                                          

@app.route('/uploadSuccess')
def uploadSuccess():
    return render_template('uploadSuccess.html')     

# Route that will process the file upload
@app.route('/upload', methods=['POST'])
def upload():
    # Get the name of the uploaded file
    file = request.files['file']
    # Check if the file is one of the allowed types/extensions
    if file and allowed_file(file.filename):
        # Make the filename safe, remove unsupported chars
        filename = secure_filename(file.filename)
        filename = str(datetime.now())+' '+filename
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        msg = Message("new inventory uploaded", sender=('SpectraPlot Info','info@spectraplot.com'), recipients=['v.miller.ii@gmail.com','mitchell.spearrin@gmail.com','christopher.strand@gmail.com','csgoldenstein@gmail.com'])
        msg.body = "New inventory posted at "+str(datetime.now())+". Filename: "+secure_filename(file.filename)+"."
        mail.send(msg)
        return redirect("/uploadSuccess")                                                                         
    


###################################
#run the app
###################################

if __name__ == "__main__":
    app.run(debug=True,threaded=True)

