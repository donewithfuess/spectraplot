# -*- coding: utf-8 -*-
"""
Created on Tue Mar 17 14:41:46 2015

@author: Chris Goldenstein and Vic Miller
"""
def Absorbance_Code_NIST_Function_v1(args):
    
    import Voigt_McLean_v1 as VF
    import Species_Function_NIST_v1 as SpF
    import numpy 
    import math
    
    h               = 6.626e-34         # J-s
    kb              = 1.38e-23          # J/molec-K
    c               = 2.99792458e10      # cm/s

    inputs          = args[0]
    kk              = args[1]
    Species_Vector  = inputs[0]
    T               = inputs[1]
    P               = inputs[2]
    Xabs_Vector     = inputs[3]
    L               = inputs[4]
    dv              = inputs[5]
    vstart          = inputs[6]
    vend            = inputs[7]
    wing_eval       = inputs[8]
    #Database        = inputs[9]
    GammaTo         = inputs[9]
    n               = inputs[10]
    Telec           = inputs[11]
    
    Species         = Species_Vector[kk]
    Xabs            = Xabs_Vector[kk]

    if Species.find('2+')>0:
        thisIon = 3
    elif Species.find('+')>0:
        thisIon = 2
    else:
        thisIon = 1
    
    v                                       = numpy.arange(vstart,vend,dv)      
    M, Q_T, SpecData, num_ions              = SpF.Species_Function_NIST_v1(Species,T,vstart,vend)
    

    vo      = SpecData[0,2]*1.0
    ii      = 0
    istart  = 0
    while vo < vstart - wing_eval and ii < len(SpecData[:,0]):
        vo      = SpecData[ii,2]
        ii      = ii + 1
        istart  = ii
        
    iend    = istart    
    ii      = istart
    while vo < vend + wing_eval and ii < len(SpecData[:,0]):
        vo      = SpecData[ii,2]
        ii      = ii + 1
        iend    = ii    
    
    if ii == len(SpecData[:,0]):
        iend = iend-1
    
    length  = len(v)
    alpha   = numpy.zeros(length)
    bad_ion = 0
    zeroEinAFlag = 0

    for ii in range(istart, iend+1):
        ion = SpecData[ii,1]
        
        if ion > num_ions and bad_ion==0:    
            print (Species)            
            print ('ion out of range')

        if ion > num_ions:
            ion = num_ions
            bad_ion = 1
            
        vo      = SpecData[ii,2]  
        EinA    = SpecData[ii,3]
        E       = SpecData[ii,4]  
        g1      = SpecData[ii,6]  
        g2      = SpecData[ii,7]

        if EinA==0:
                zeroEinAFlag = 1
                EinA = 10000000.0

        lamb    = 1/vo
        nX      = Xabs*P*101325./(kb*T)*(0.01)**3
        fi      = g1*numpy.exp(-h*c*E/(kb*Telec))/Q_T[ion-1]
        S_T     = lamb**2/(8*numpy.pi)*nX*fi*EinA*g2/g1*(1-numpy.exp(-h*c*vo/(kb*Telec)))/c
        

        if ion != thisIon:
            S_T = 0

        A       = S_T*L

        GammaT = GammaTo*(296/T)**n        
        vd     = vo*7.1623*10**(-7)*math.sqrt(T/M)
        vc     = 2*P*GammaT
        
        phiV   = VF.Voigt_McLean_v1(v, vo, vd, vc, dv, wing_eval)     
        
        alpha  = A*phiV + alpha
    return alpha, zeroEinAFlag




