#config.py

PROPAGATE_EXCEPTIONS = True
DEBUG = True
THREADED = True
SQLALCHEMY_DATABASE_URI = 'mysql://spectraplot:password@spectraplot.cq60ipps6rt2.us-west-1.rds.amazonaws.com/HITRAN'

MAIL_SERVER = "smtp.zoho.com"
MAIL_PORT = 465
MAIL_USE_SSL = True
MAIL_USERNAME = 'info@spectraplot.com'
MAIL_PASSWORD = 'spectroscopyisfun'

UPLOAD_FOLDER = './vendors/'
# These are the extension that we are accepting to be uploaded
ALLOWED_EXTENSIONS = set(['csv', 'xls','xlsx'])