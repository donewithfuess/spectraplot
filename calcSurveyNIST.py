def calcSurveyNIST(T,vstart,vend,Species_Vector,Xabs_Vector):

	import Linestrength_Collector_NIST as ST
	import numpy

######################################################################################################################

	

########################################################################################################################

	num_species     = len(Species_Vector)
	inputs          = Species_Vector, T, vstart, vend, Xabs_Vector
	strengths = {}
	edoubles  = {}

	for ii in range(0,num_species):		
		strengths[ii]    = ST.Linestrength_Collector_NIST([inputs,ii])  

	#return strengths, edoubles
	return strengths

