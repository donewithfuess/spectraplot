# -*- coding: utf-8 -*-
"""
Created on Wed Jul 30 20:48:58 2014

@author: Chris
"""
def Q_T_Function_NIST_v1(col,num_ions,T,Q_Tv,Tv):
    import numpy    
    Q_Tvect         = numpy.zeros(num_ions)
    length_T        = len(Tv)
    if T < Tv[0]:
        T_L             = Tv[0]
        T_H             = Tv[1]
        Q_Tvect         = Q_Tv[0] + (T-T_L)*( Q_Tv[1]-Q_Tv[0] )/(T_H-T_L)
    elif T>= Tv[length_T-1]:
        T_L             = Tv[length_T-2]
        T_H             = Tv[length_T-1]
        Q_Tvect         = Q_Tv[length_T-2] + (T-T_L)*( Q_Tv[length_T-1]-Q_Tv[length_T-2] )/(T_H-T_L)
    else:               
        for tt in range(0,length_T):
            if tt < length_T and Tv[tt] <= T and Tv[tt+1] >= T:
                T_L             = Tv[tt]
                T_H             = Tv[tt+1]
                Q_Tvect         = Q_Tv[tt] + (T-T_L)*( Q_Tv[tt+1]-Q_Tv[tt] )/(T_H-T_L)
                
    return Q_Tvect