$(function() {
////////////////////////////////		
// Initialize some variables		
////////////////////////////////		
var numPlots  = 0;  //to track how many sets of lines (ie, plots) are on the plot
var numLines  = 0;	//to track how many lines have been plotted in each plot 
var numColor  = 0;  //to track which color to plot
var firstPlot = 0;  //is this the first plot?
var colors = ["blue","orange","purple","magenta","cyan","black","red","green","yellow","gray"];
var lineStyles = ["0","2,2","3,1"];  //not used in line survey
var formatNu2dec  = d3.format("4.7g");  //format for nu
var formatAbs3dec = d3.format("3.3e");  //format for other numbers
var lineStrings = ["none"];
var margin = {top: 60, right: 30, bottom: 150, left: 90},  //dimensions for plot
    margin2 = {top: 380, right: 30, bottom: 20, left: 90},
    width = 900 - margin.left - margin.right,
    height = 450 - margin.top - margin.bottom/1.5,
    height2 = 450 - margin2.top - margin2.bottom;
var btn_hitran = $('#calculate_hitran');
var btn_nist = $('#calculate_nist');

var lineHolder = [];
var	Scutoff = $('input[name="Scutoff"]').val();  //Linestrength cutoff value
var cookieName_hitran = 'spectraPlotSurveyParameters_hitran';  //name of the cookie
var cookieName_nist = 'spectraPlotSurveyParameters_nist';  //name of the cookie

//var nuHolder = [];  
//var absHolder = [];

var lineLimit = 7500;  //if more than lineLimit lines are plotted, stop plotting and throw the flag
var lineFlag  = 0;     //flag to be thrown if lineLimit is reached
var ylimStore;		   //store the limits on the yaxis

////////////////////////////////		
// Start building the SVG
////////////////////////////////		

//variables for all the x and y axes
var x      = d3.scale.linear().range([0, width]),
    x2     = d3.scale.linear().range([0, width]),
    xtop   = d3.scale.pow().exponent(-1).range([0, width]),
    y      = d3.scale.log().range([height, 0]),
    y2     = d3.scale.log().range([height2, 0]),
    yright = d3.scale.log().range([height, 0]);
    
    y.domain([Scutoff/2, 1]);
    yright.domain(y.domain());
    y2.domain(y.domain());
    

//function for getting max y in brush window, 
//and used for finding the location of the cursor in the nu array
var bisect = d3.bisector(function(d) { return d.nu; }).left;


//set up axes and formats
var xAxis       = d3.svg.axis().scale(x).orient("bottom"),
    xAxis2      = d3.svg.axis().scale(x2).orient("bottom"),
    xAxisTop    = d3.svg.axis().scale(xtop).orient("top"),
    yAxis       = d3.svg.axis().scale(y).orient("left").ticks(14),
    yAxisRight  = d3.svg.axis().scale(y).orient("right");

xAxis.tickFormat(function(e){return d3.round(e,7)});
//yAxis.tickFormat(d3.format("3.g"));
//yAxis.tickFormat(function(e){return y.tickFormat(3,d3.format("2.0e"))(e)});
//yAxis.ticks(0);
//yAxis.ticks(5);
//yAxis.tickFormat(d3.format("2.0e"));
yAxisRight.tickFormat("");
xAxis2.tickFormat(d3.format("4.g"));
xAxisTop.tickFormat(function(e){return d3.round(e,7)});
    xtop.domain([10,5]);
//this is the brush
var brush = d3.svg.brush()
    .x(x2)
    .on("brush", brushed);

//this is the focus line function
var line = d3.svg.line()
    .x(function(d) { return x(d.nu); })
    .y(function(d) { return y(d.abs); });

//this is the context line function
var line2 = d3.svg.line()
	.x(function(d) { return x2(d.nu); })
	.y(function(d) { return y2(d.abs); });

//this is the svg background
var svg = d3.select("#chart").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .attr("id","plot");
    
//clipping path for focus
svg.append("defs").append("clipPath")
    .attr("id", "clip")
    .append("rect")
    .attr("width", width)
    .attr("height", height);

//clipping path for context    
svg.append("defs").append("clipPath")
    .attr("id", "contextclip")
    .append("rect")
    .attr("width", width)
    .attr("height", height2);    
    
//arrowheads for the overlay on page load    
svg.append("defs").append("marker")
    .attr("id", "arrowhead")
    .attr("refX",'1.5') /*must be smarter way to calculate shift*/
    .attr("refY",'3.5')
    .attr("markerWidth",'8')
    .attr("markerHeight",'8')
    .attr("orient", "auto")
    .append("path")
    .attr("d", "M0,0 L8,3.5 L0,7")
    .attr("fill","none")
    .attr("stroke","black");
    	
//append the focus to the SVG	
var focus = svg.append("g")
    .attr("class", "focus")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

//append the context to the SVG
var context = svg.append("g")
    .attr("class", "context")
    .attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");

//append the legend to the SVG
var legend = svg.append("g")
    .attr("class", "legend");

//append the overlay (on page load) to the svg
var introGroup = svg.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");;        

//append the the brush to the context
context.append("g")
    .attr("class", "x brush")
    .call(brush)
    .selectAll("rect")
    .attr("y", -6)
    .attr("height", height2 + 7);

//append x axis to the focus
focus.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis)
    .attr("font-size","13px")
    .attr("font-family", "sans-serif");

//append top x axis (microns) to the focus
var topaxis = focus.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0,0)")
    .call(xAxisTop)
    .attr("font-size","13px")
    .attr("font-family", "sans-serif");

//append left y axis (microns) to the focus
focus.append("g")
    .attr("class", "y axis")
    .call(yAxis)
    .attr("font-size","13px")
    .attr("font-family", "sans-serif"); 

//append right y axis (microns) to the focus    
var rightaxis = focus.append("g")
    .attr("class", "y axis")
    .attr("transform", "translate("+width+",0)")
    .call(yAxisRight)
    .attr("font-size","13px")
    .attr("font-family", "sans-serif");     

//now append x axis to the context    
context.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height2 + ")")
    .call(xAxis2)
    .attr("font-size","13px")
    .attr("font-family", "sans-serif");
    
//add the axis titles - this one is wavenumber on focus.x
context.append("text")
    .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
    .attr("transform", "translate("+ (width/2) +","+(height2+margin2.bottom*2)+")")  // centre below axis
    .text("Frequency, cm⁻¹")
    .attr("font-size", "15px")
    .attr("font-family", "sans-serif");	
    
//y axis title, focus.y
focus.append("text")
	.attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
    .attr("transform", "translate("+ (-margin.left/1.5) +","+(height/2)+")rotate(-90)")  // text is drawn off the screen top left, move down and out and rotate
    .text("\u03C7i * Linestrength, cm⁻²/atm")
    .attr("font-size", "15px")
    .attr("font-family", "sans-serif");  

//x top (wavelength) axis title, focus.xtop
focus.append("text")
	.attr("text-anchor","middle")
	.attr("transform","translate("+ (width/2) +", -30)")
	.text("Wavelength, \u03BCm")
	.attr("font-size", "15px")
	.attr("font-family", "sans-serif");      
    
//add the tooltip, which is the text that goes with the cursor and displays
//the E'', nu_o, etc.    
svg.append("text")
		  .attr("id", "tooltip")
		  .attr("x", .13*width)
		  .attr("y", 75)//height+height2+50+4)
		  .attr("text-anchor", "right")
		  .attr("font-family", "sans-serif")
		  .attr("font-size", "11px")
		  .attr("font-weight", "bold")
		  .attr("fill", "black");
		  
svg.append("text")
		  .attr("id", "tooltip2")
		  .attr("x", .13*width)
		  .attr("y", 90)//height+height2+50+4)
		  .attr("text-anchor", "right")
		  .attr("font-family", "sans-serif")
		  .attr("font-size", "11px")
		  .attr("font-weight", "bold")
		  .attr("fill", "black");
		  
svg.append("text")
		  .attr("id", "tooltip3")
		  .attr("x", .13*width)
		  .attr("y", 105)//height+height2+50+4)
		  .attr("text-anchor", "right")
		  .attr("font-family", "sans-serif")
		  .attr("font-size", "11px")
		  .attr("font-weight", "bold")
		  .attr("fill", "black");		  		  
		
//append the cursor to the focus
var hoverLine = focus.append("circle")
  .attr('r', 7)
  .attr("class","hover-line")
  //.style('display', 'none')
  .attr('cx',-100)
  .attr('cy',1e6)
  .style('fill', '#FFFFFF')
  .style('pointer-events', 'none')
  .style('stroke', '#FB5050')
  .style('stroke-width', '3px')
  .style("z-index","100");

////////////////////////////////		
//Done with all the figure/plotting setup.  Now for the function to submit the form fields
//to the back end.
////////////////////////////////		
	
	
// function for plotting	
var submit_form = function(e){
	var simType = e.data.simType;
	lineFlag  = 0;  //flag for too many lines plotted
	Scutoff = $('input[name="Scutoff"]').val();  //Linestrength cutoff value
	//console.log(Scutoff);

	// check for isotope checkbox
	var isotope_include = 0;  
	if ($('input[name="isotopes"]').is(':checked')) {isotope_include = 1;}
	
	// clear out demo text/picture on running first simulation
	if(firstPlot==1){clear_plot();}
	
	// for first plot, get rid of first image/group that's been plotted
	if(firstPlot>0){introGroup.attr("visibility","hidden");}

	//disable calculate button
	btn_hitran.button('loading');
    btn_nist.button('loading');
    

	//initialize number of lines tracker, and see how many species are going to be calculated
	numLines = 0;
	if ($("#spec1_"+simType).find('.specspan').text()=='Species') {
		species1 = null;
		species1HTML = "Species";
		xabs1    = null;}
	else { 
		species1 = $("#spec1_"+simType).find('.specspan').text();
		species1HTML = $("#spec1_"+simType).find('.specspan').html();
		xabs1    = $('input[name="xspecies1_'+simType+'"]').val();}
		
	if ($("#spec2_"+simType).find('.specspan').text()=='Species') {
		species2 = null;
		species2HTML = "Species";		
		xabs2    = null;}
	else { 
		species2 = $("#spec2_"+simType).find('.specspan').text();
		species2HTML = $("#spec2_"+simType).find('.specspan').html();		
		xabs2    = $('input[name="xspecies2_'+simType+'"]').val();}	

	if ($("#spec3_"+simType).find('.specspan').text()=='Species') {
		species3 = null;
		species3HTML = "Species";		
		xabs3    = null;}
	else { 
		species3 = $("#spec3_"+simType).find('.specspan').text();
		species3HTML = $("#spec3_"+simType).find('.specspan').html();		
		xabs3    = $('input[name="xspecies3_'+simType+'"]').val();}

	//assemble simulation parameters			
	speciesArray = [species1,species2,species3];
	speciesArrayHTML = [species1HTML,species2HTML,species3HTML];
	speciesArray = speciesArray.filter(function(n){ return n != undefined });

	xArray = [xabs1,xabs2,xabs3];
	xArrayTotal = xArray;
	xArray = xArray.filter(function(n){ return n != undefined });
	
	T = $('input[name="T_hitran"]').val();
	P = $('input[name="P_hitran"]').val();
	vstart 	= $('input[name="vstart_'+simType+'"]').val();
	vend 	= $('input[name="vend_'+simType+'"]').val();
	Telec = $('input[name="Telec_nist"]').val();
	cook   	= $.cookie('_ga');

	var toobig = 0;
	
		if ((vend-vstart>3000 || vstart-vend>3000) && simType=="hitran") { 
			$('#toobigDiv').show();
			toobig = 1;
			} 
	
	
	if (toobig == 1) {return;}
	
	if (Scutoff <= 1e-20) {$('#surveyZeroCutoff').show();return;}
	
	// if hitran and out of IR, throw flag
	if ((vstart > 10000 || vend > 10000) && simType=="hitran") {
		$('#notinfraredDiv').show();
	}	

	// if your mole fractions are greater than 1, throw a flag, disable buttons, and return
	var molefracsTooBig = 0;
	// set molefracsTooBig
	xArray.forEach(function(d){
		if (d>1) { 
			$('#molefracSurveyDiv').show();
			//btn_hitran.button('disabled');
			//btn_nist.button('disabled');
			molefracsTooBig = 1;
			} 
		});
	if (molefracsTooBig == 1) {return;}

	var badspecies = 0;
		if ((vend-vstart>1000 || vstart-vend>1000) && speciesArray.indexOf("CO2 HITEMP")>=0) { 
			$('#badspecies').show();
		}
	if (badspecies == 1) {return;}
		if ((vend-vstart>1000 || vstart-vend>1000) && speciesArray.indexOf("H2O HITEMP")>=0) { 
			$('#badspecies').show();
		}
	if (badspecies == 1) {return;}
	
	
	if(firstPlot==0){
		T = 300;
		P = 1;
		L = 100;
		vstart = 1500;
		vend = 7000;
		deltav = 0.0075;
		xArray=[1];
		Scutoff = 1e-5;
		speciesArray=['CO'];}
		
	//console.log(isotope_include);
	
/////// Here's the AJAX call to the python script	
       $.ajax({
		 url:	$SCRIPT_ROOT + '/_calcSurvey',
		data: {T: T,
			   P: P,
 isotope_include: isotope_include,
          vstart: vstart,
            vend: vend,
            scut: Scutoff,
         species: speciesArray,
       molefracs: xArray,
           Telec: Telec,
   firstPlotFlag: firstPlot,
            cook: cook,
         simType: simType},

	 success: function(data) { 
				//turn calculate button back on
				console.log('back in success loop');
				btn_hitran.button('reset');
	 			btn_nist.button('reset');

				//console.log(data);
				//console.log(data.output);
	 			//for every piece of data returned, make sure they're numbers
			Object.keys(data.output).forEach(function(val) {
	 				data.output[val].forEach(function(d) {
	    					d.nu  = +d.nu;
    						d.abs = +d.abs; //strength
    						d.edb = +d.edb;
    						d.iso = +d.iso;
    						d.gair = +d.gair;
    						d.gslf = +d.gslf;
    						d.nair = +d.nair;});
    				//data.edoubles[val].forEach(function(d) {
    						//d.nu  = +d.nu;
    						//d.abs = +d.abs;});

			// if you've already plotted a line, check to see if domain is bigger or smaller than existing domain
			if (numLines > 0 || numPlots > 0) {		
			  	newxdomain = d3.extent(data.output[val], function(d) { return d.nu; });
			  	newydomain = 2*d3.max(data.output[val], function(d) { return d.abs; });
				oldmax = d3.max([newydomain, y.domain()[1]]);

				//find bounds from new domain and old domain by concatenating them - then taking extent
			  	x.domain(d3.extent(newxdomain.concat(x.domain())));
			 	y.domain([Scutoff/2, oldmax]);}
			// otherwise set the domain from the first returned data
		  	else {
		  		xcheck = d3.extent(data.output[val], function(d) { return d.nu; });
		  		xcheck.push(+$('input[name="vstart_'+simType+'"]').val());
		  		xcheck.push(+$('input[name="vend_'+simType+'"]').val());
		  		//console.log(xcheck);
		  		x.domain(d3.extent(xcheck));
		  		//x.domain(d3.extent(data.output[val], function(d) { return d.nu; }));
			  	y.domain([Scutoff/2, 2*d3.max(data.output[val], function(d) { return d.abs; })]);
			  	}
			  	
		  // now we're outside of focus-bound finding
		  x2.domain(x.domain());
		  y2.domain(y.domain());
		  yright.domain(y.domain());

		  //store the y limits so the view changes back to the original when the 
		  //autoscaleY checkbox is ticked and unticked
		  ylimStore = y.domain();  


		  //draw the lines
		  var lineCount = 0;
		  //console.log(data.output[val]);
		  //console.log(x.domain());
		  data.output[val].forEach(function(d){
		  	  
		  	  if (lineCount < lineLimit) {
		  	    
		  	    if (d.abs < Scutoff && simType=='nist'){
		  	    	lineCount++;	
		  	    	lineHolder.push({nu: d.nu, abs: d.abs, edb: d.edb, iso:d.iso, gair:d.gair, gslf:d.gslf, nair:d.nair, line: numColor});
		  	  	var stick = [{nu: d.nu, abs: Scutoff*1.1},{nu: d.nu, abs: 1e-20}];
		  	  focus.append("path")
				  .datum(stick)
			  	  .attr("d", line)
			  	  .attr("stroke",colors[numColor])
				  .attr("stroke-dasharray",lineStyles[0])
				  .attr("class","line")
				  .attr("opacity","0.7");

			  context.append("path")
		  		.datum(stick)
		  		.attr("d", line2)
			    .attr("stroke",colors[numColor])
			    .attr("stroke-dasharray",lineStyles[0])
			    .attr("class", "line")
			    .attr("opacity","0.7");
		  	    }
		  	    else if(d.abs > Scutoff){
		  	    lineCount++;
		  	  	//nuHolder.push(d.nu);
		  	  	//absHolder.push(d.abs);
		  	  	lineHolder.push({nu: d.nu, abs: d.abs, edb: d.edb, iso:d.iso, gair:d.gair, gslf:d.gslf, nair:d.nair, line: numColor});
		  	  	var stick = [{nu: d.nu, abs: d.abs},{nu: d.nu, abs: 1e-20}];
		  	  focus.append("path")
				  .datum(stick)
			  	  .attr("d", line)
			  	  .attr("stroke",colors[numColor])
				  .attr("stroke-dasharray",lineStyles[0])
				  .attr("class","line")
				  .attr("opacity","0.7");

			  context.append("path")
		  		.datum(stick)
		  		.attr("d", line2)
			    .attr("stroke",colors[numColor])
			    //.attr("stroke-dasharray",lineStyles[0])
			    .attr("class", "line")
			    .attr("opacity","0.7");
			    }
			  }
			  });
			  
			  if (lineCount >= lineLimit){
			  	lineFlag = 1;
			  	}
			  

        //construct legend
        legend.append("line")
			.attr("x1", width - 70)
          	.attr("y1", 90+(val*20)+(numPlots*80))
			.attr("x2", width - 60)
          	.attr("y2", 100+(val*20)+(numPlots*80))
           	.attr("width", 10)
          	.attr("height", 10)
          	.attr("stroke-width", 2)
            .attr("stroke", colors[numColor])
            .attr("class","labels");
          		 
        legend.append("text")
          	.attr("x", width - 50)
          	.attr("y", 90+(val*20)+10+(numPlots*80))
          	.attr("height",30)
          	.attr("width",100)
          	.style("fill", colors[numColor])
          	.attr("class","labels")
	        .text(speciesArray[val]+': \u03C7 = '+xArray[val])
	        .attr("font-family", "sans-serif")
	        .attr("font-size", "12px");

	    if(numPlots==0 && val < 1){
	    legend.append("text")
	    	.attr("x", width-20)
	    	.attr("y", height+height2)
	    	.attr("class","labels")
	    	.text("SpectraPlot.com")
	    	.attr("font-family", "sans-serif")
	    	.attr("font-size", "14px");}


		  //update all the axes/lines except wavelength and right-side axis
		  context.select(".x.axis").call(xAxis2);
		  focus.select(".x.axis").call(xAxis);
		  focus.select(".y.axis").call(yAxis);
		  context.selectAll(".line").attr("d",line2);
		  focus.selectAll(".line").attr("d",line);

	      thisLineString = speciesArray[val] + '/x=' + xArray[val] + '/T=' + T + 'K/P=' + P + 'atm/L=' + L + 'cm';
		  lineStrings.push(thisLineString);
		  numLines++;  	
		  numColor++;
	           	
		  		}); //loop around all datasets returned from simulator

		lineHolder.sort(function(a, b) { return a.nu - b.nu; });
		//console.log(lineHolder);	
		if(simType=='hitran'){
        legend.append("text")
			.attr("x", width - 75)
        	.attr("y", 70+(numPlots*80)+10)
		    .attr("height",30)
		    .attr("width",100)
		    .attr("class","labels")
		    .style("fill", "black")
			.text("T = "+T+"K, P = "+P+" atm")
			.attr("font-family", "sans-serif")
	        .attr("font-size", "12px");
	    }
	    else{
        legend.append("text")
			.attr("x", width - 75)
        	.attr("y", 70+(numPlots*80)+10)
		    .attr("height",30)
		    .attr("width",100)
		    .attr("class","labels")
		    .style("fill", "black")
			.text("Telec = "+Telec+"K")
			.attr("font-family", "sans-serif")
	        .attr("font-size", "12px");
	    }
		
		
	   focus.append("rect")
		  	.attr("class", "overlay")
      		.attr("width", width)
     		.attr("height", height)
    		.on("mouseout", function() { hoverLine.style("opacity",1e-6);
    									 d3.select("#tooltip").style("opacity",1e-6);
    									 d3.select("#tooltip2").style("opacity",1e-6);
    									 d3.select("#tooltip3").style("opacity",1e-6); })

		    .on("mousemove", function(d) { 
					if (lineHolder){
						var x0 = x.invert(d3.mouse(this)[0]);
						var xPt  = bisect(lineHolder,x0,0);
						if (xPt==lineHolder.length){xPt=xPt-1;}
				
						var totalAbs = lineHolder[xPt].abs;
						var edoubleprime = lineHolder[xPt].edb;

						//add the vertical line text
						if(lineHolder[xPt].iso>=0){
			d3.select("#tooltip")			
				.text("\u03BD="+formatNu2dec(lineHolder[xPt].nu)+" cm⁻¹, S="+formatAbs3dec(totalAbs)+ " cm⁻²/atm, E''="+formatAbs3dec(edoubleprime)+" cm⁻¹")
				.style("opacity",1)
				.style("fill",colors[lineHolder[xPt].line]);
			d3.select("#tooltip2")			
				.text("\u03B3 air="+lineHolder[xPt].gair+", \u03B3 self="+lineHolder[xPt].gslf+" cm^-1/atm (at 296K)")
				.style("opacity",1)
				.style("fill",colors[lineHolder[xPt].line]);
			d3.select("#tooltip3")			
				.text("n air="+lineHolder[xPt].nair+", HITRAN isotope number: "+lineHolder[xPt].iso)
				.style("opacity",1)
				.style("fill",colors[lineHolder[xPt].line]);				
        	hoverLine
				.attr("cx",x(lineHolder[xPt].nu))
				.attr("cy",y(totalAbs))
				.attr("clip-path","url(#clip)")
				.style("opacity",1)
				.style("stroke",colors[lineHolder[xPt].line]);
				}
				else{
			d3.select("#tooltip")			
				.text("\u03BD="+formatNu2dec(lineHolder[xPt].nu)+" cm⁻¹, S="+formatAbs3dec(totalAbs)+ " cm⁻²/atm")
				.style("opacity",1)
				.style("fill",colors[lineHolder[xPt].line]);
			d3.select("#tooltip2")			
				.text("E'="+formatAbs3dec(lineHolder[xPt].gair)+", E''="+formatAbs3dec(edoubleprime)+" cm⁻¹")
				.style("opacity",1)
				.style("fill",colors[lineHolder[xPt].line]);
			d3.select("#tooltip3")			
				.text("g upper="+lineHolder[xPt].nair+", g lower="+lineHolder[xPt].gslf)
				.style("opacity",1)
				.style("fill",colors[lineHolder[xPt].line]);				
        	hoverLine
				.attr("cx",x(lineHolder[xPt].nu))
				.attr("cy",y(totalAbs))
				.attr("clip-path","url(#clip)")
				.style("opacity",1)
				.style("stroke",colors[lineHolder[xPt].line]);

				}

							//move vertical line
			    		mousex = d3.mouse(this)[0];
				    		hoverLine
				 			.attr("cx",x(lineHolder[xPt].nu))
							.attr("cy",y(totalAbs))
							.attr("clip-path","url(#clip)")
							.style("opacity",1)
							.style("stroke",colors[lineHolder[xPt].line]);
						}  //if plotData is not null
				})  //finish mouse move
		  	
			numPlots++;
			//on page load, put up the fancy picture
			if(firstPlot==0){
				
				x2.domain([1500,7000]);
				displayIntroGroup(introGroup,width,height);
				brush.extent([4000,4400]);
				svg.select(".brush").call(brush);
				brushed();
				context.select(".x.axis").call(xAxis2);
			}
			
			
			if(firstPlot>0){
				if(simType=="hitran"){
				$.cookie(cookieName_hitran, [T,P,vstart,vend,Scutoff,speciesArrayHTML,xArrayTotal]);
				}
				else{
				$.cookie(cookieName_nist, [Telec,vstart,vend,speciesArrayHTML,xArrayTotal]);
				
					if (typeof $.cookie(cookieName_hitran) === 'undefined'){
					//no hitran cookie
					} else {
						cookieArray = $.cookie(cookieName_hitran).split(",");
						cookieArray[4] = Scutoff;
						$.cookie(cookieName_hitran, cookieArray);
					}
				}


				buildTables(data);
				$(document.body).animate({
		    		'scrollTop':   $('#topofplot').offset().top
					}, 500);
				}
			
			firstPlot++;
			
			if(numPlots>=3){
				btn_hitran.button('disabled');
				btn_nist.button('disabled');
			}
			
			
			//now set lambda x-axis
			lowLambda = 10000/d3.min(x.domain());
			highLambda = 10000/d3.max(x.domain());
			
			xtop.domain([lowLambda, highLambda]);

			topaxis.call(xAxisTop);
			rightaxis.call(yAxisRight);
			brushed();
			
			if (lineFlag==1){
				$('#reduceLinesDiv').show();
			}

		}, //close success

	error: function(jqXHR, textStatus, errorThrown) {
		if(textStatus==="timeout") {
			$('#timeoutDiv').show();    
		    } 
		else{
			$('#alertDiv').show();
			}
		},
	//timeout: 30000
				});  //close ajax call
    		};  //close submit_form
    
    
var clear_plot = function(e) {    
    focus.selectAll(".line").remove();
    context.selectAll(".line").remove();
    numPlots = 0;
    numColor = 0;
    d3.selectAll(".brush").call(brush.clear());
    legend.selectAll(".labels").remove();
    lineStrings = [];
    lineHolder = [];
    lineFlag = 0;
    btn_hitran.button('reset');
    btn_nist.button('reset');
	introGroup.attr("visibility","hidden");
    $('.alert-dismissible').hide();
    hoverLine.style("opacity",1e-6);
	d3.select("#tooltip").style("opacity",1e-6);
	d3.select("#tooltip2").style("opacity",1e-6);
	d3.select("#tooltip3").style("opacity",1e-6);
}




function brushed() {
	// get domain
	x.domain(brush.empty() ? x2.domain() : brush.extent());
	
	// if y zoom box checked, autozoom y
	if ($('input[name="autoscaleY"]').is(':checked')) {	
		var data = [];
	    for (i in focus.selectAll(".line").data()) {
    		data = data.concat(focus.selectAll(".line").data()[i]);
	    }

    var dataFiltered = data.filter(function(d, i) {
    	if ( (d.nu >= x.domain()[0]) && (d.nu <= x.domain()[1]) ) {
      		return d.abs;
    		}
		})
  	y.domain([Scutoff/2, 2*d3.max(dataFiltered.map(function(d) { return d.abs; }))]);
  }
  else { 
  		if($('input[name="fixY"]').is(':checked')) {
  		y.domain([Scutoff/2,$('input[name="fixYval"]').val()]);
  		}
  		else{y.domain(ylimStore);}
  	}

  		//put new ticks for cm-1 that correspond to um
  		var newticks = x.ticks(5);
  		for (var i = 0;i<newticks.length;i++){
  			newticks[i] = 10000/newticks[i];
  			}
		xAxisTop.tickValues(newticks);

  focus.selectAll(".line").attr("d", line).style("clip-path","url(#clip)");
  context.selectAll(".line").attr("d",line2).style("clip-path","url(#contextclip)");

  focus.select(".x.axis").call(xAxis);
  focus.select(".y.axis").call(yAxis);
  rightaxis.call(yAxisRight);
  lowLambda = 10000/d3.min(x.domain());
  highLambda = 10000/d3.max(x.domain());

			
  xtop.domain([lowLambda, highLambda]);
  topaxis.call(xAxisTop);
}


function autoscaleYclick() {
	$('input[name="fixY"]').attr('checked',false);
	brushed();
	}
	
function fixYscale() {
	$('input[name="autoscaleY"]').attr('checked',false);
	brushed();
	}	


function startcm2um() {
	var cm = $('input[name="lstart_nist"]').val();
	var um = 10000/cm;	
	$('input[name="vstart_nist"]').val(d3.round(um,5));
	var cm = $('input[name="lstart_hitran"]').val();
	var um = 10000/cm;	
	$('input[name="vstart_hitran"]').val(d3.round(um,5));
	}	

function endcm2um() {
	var cm = $('input[name="lend_nist"]').val();
	var um = 10000/cm;	
	$('input[name="vend_nist"]').val(d3.round(um,5));
	var cm = $('input[name="lend_hitran"]').val();
	var um = 10000/cm;	
	$('input[name="vend_hitran"]').val(d3.round(um,5));
	}	
	
function startum2cm() {
	var cm = $('input[name="vstart_nist"]').val();
	var um = 10000/cm;	
	$('input[name="lstart_nist"]').val(d3.round(um,5));
	var cm = $('input[name="vstart_hitran"]').val();
	var um = 10000/cm;	
	$('input[name="lstart_hitran"]').val(d3.round(um,5));
	}	

function endum2cm() {
	var cm = $('input[name="vend_nist"]').val();
	var um = 10000/cm;	
	$('input[name="lend_nist"]').val(d3.round(um,5));
	var cm = $('input[name="vend_hitran"]').val();
	var um = 10000/cm;	
	$('input[name="lend_hitran"]').val(d3.round(um,5));
	}				

$('#autoscaleY').bind('click', autoscaleYclick);
$('#fixY').bind('click', fixYscale);
$('#fixYval').bind('change', fixYscale);
$("button#calculate_hitran").bind("click",{simType: "hitran"}, submit_form);
$("button#calculate_nist").bind("click",{simType: "nist"}, submit_form);
$('button#clear').bind('click', clear_plot);


$('input[name=T]').focus();
$('input[name=lstart_nist]').bind('keyup',startcm2um);
$('input[name=lend_nist]').bind('keyup',endcm2um);

$('input[name=vstart_nist]').bind('keyup',startum2cm);
$('input[name=vend_nist]').bind('keyup',endum2cm);

$('input[name=lstart_hitran]').bind('keyup',startcm2um);
$('input[name=lend_hitran]').bind('keyup',endcm2um);

$('input[name=vstart_hitran]').bind('keyup',startum2cm);
$('input[name=vend_hitran]').bind('keyup',endum2cm);

d3.select("#save_as_png").on("click", function(){
    saveSvgAsPng(document.getElementById("plot"), "spectraplot.png",{scale: 2});
    });

$(document).keydown(function(e) {
    switch(e.which) {
        case 37: // left
        //console.log('left!');
        //console.log(x.invert(hoverLine.attr("cx")));
        	var x0   = x.invert(hoverLine.attr("cx"))-.00001;
        	var xPt  = bisect(lineHolder,x0,0);
        	xPt = xPt-1;
			if (xPt==lineHolder.length){xPt=xPt-1;}
			var totalAbs = lineHolder[xPt].abs;
			var edoubleprime = lineHolder[xPt].edb;

			//add the vertical line text
			if(lineHolder[xPt].iso>=0){
			d3.select("#tooltip")			
				.text("\u03BD="+formatNu2dec(lineHolder[xPt].nu)+" cm⁻¹, S="+formatAbs3dec(totalAbs)+ " cm⁻²/atm, E''="+formatAbs3dec(edoubleprime)+" cm⁻¹")
				.style("opacity",1)
				.style("fill",colors[lineHolder[xPt].line]);
			d3.select("#tooltip2")			
				.text("\u03B3 air="+lineHolder[xPt].gair+", \u03B3 self="+lineHolder[xPt].gslf+" cm^-1/atm (at 296K)")
				.style("opacity",1)
				.style("fill",colors[lineHolder[xPt].line]);
			d3.select("#tooltip3")			
				.text("n air="+lineHolder[xPt].nair+", HITRAN isotope number: "+lineHolder[xPt].iso)
				.style("opacity",1)
				.style("fill",colors[lineHolder[xPt].line]);				
        	hoverLine
				.attr("cx",x(lineHolder[xPt].nu))
				.attr("cy",y(totalAbs))
				.attr("clip-path","url(#clip)")
				.style("opacity",1)
				.style("stroke",colors[lineHolder[xPt].line]);
				}
				else{
			d3.select("#tooltip")			
				.text("\u03BD="+formatNu2dec(lineHolder[xPt].nu)+" cm⁻¹, S="+formatAbs3dec(totalAbs)+ " cm⁻²/atm")
				.style("opacity",1)
				.style("fill",colors[lineHolder[xPt].line]);
			d3.select("#tooltip2")			
				.text("E'="+formatAbs3dec(lineHolder[xPt].gair)+", E''="+formatAbs3dec(edoubleprime)+" cm⁻¹")
				.style("opacity",1)
				.style("fill",colors[lineHolder[xPt].line]);
			d3.select("#tooltip3")			
				.text("g upper="+lineHolder[xPt].nair+", g lower="+lineHolder[xPt].gslf)
				.style("opacity",1)
				.style("fill",colors[lineHolder[xPt].line]);				
        	hoverLine
				.attr("cx",x(lineHolder[xPt].nu))
				.attr("cy",y(totalAbs))
				.attr("clip-path","url(#clip)")
				.style("opacity",1)
				.style("stroke",colors[lineHolder[xPt].line]);

				}
				
			//console.log(hoverLine.attr("cx"));
			//console.log(hoverLine.attr("cy"));
				
				
        break;

        //case 38: // up
        //break;

        case 39: // right
            var x0   = x.invert(hoverLine.attr("cx"));
        	var xPt  = bisect(lineHolder,x0,0);
        	//console.log(xPt);
        	xPt = xPt+1;
        	//console.log(xPt);
			if (xPt==lineHolder.length){xPt=xPt-1;}
			var totalAbs = lineHolder[xPt].abs;
			var edoubleprime = lineHolder[xPt].edb;

			//add the vertical line text
			if(lineHolder[xPt].iso>=0){
			d3.select("#tooltip")			
				.text("\u03BD="+formatNu2dec(lineHolder[xPt].nu)+" cm⁻¹, S="+formatAbs3dec(totalAbs)+ " cm⁻²/atm, E''="+formatAbs3dec(edoubleprime)+" cm⁻¹")
				.style("opacity",1)
				.style("fill",colors[lineHolder[xPt].line]);
			d3.select("#tooltip2")			
				.text("\u03B3 air="+lineHolder[xPt].gair+", \u03B3 self="+lineHolder[xPt].gslf+" cm^-1/atm (at 296K)")
				.style("opacity",1)
				.style("fill",colors[lineHolder[xPt].line]);
			d3.select("#tooltip3")			
				.text("n air="+lineHolder[xPt].nair+", HITRAN isotope number: "+lineHolder[xPt].iso)
				.style("opacity",1)
				.style("fill",colors[lineHolder[xPt].line]);				
        	hoverLine
				.attr("cx",x(lineHolder[xPt].nu))
				.attr("cy",y(totalAbs))
				.attr("clip-path","url(#clip)")
				.style("opacity",1)
				.style("stroke",colors[lineHolder[xPt].line]);
				}
				else{
			d3.select("#tooltip")			
				.text("\u03BD="+formatNu2dec(lineHolder[xPt].nu)+" cm⁻¹, S="+formatAbs3dec(totalAbs)+ " cm⁻²/atm")
				.style("opacity",1)
				.style("fill",colors[lineHolder[xPt].line]);
			d3.select("#tooltip2")			
				.text("E'="+formatAbs3dec(lineHolder[xPt].gair)+", E''="+formatAbs3dec(edoubleprime)+" cm⁻¹")
				.style("opacity",1)
				.style("fill",colors[lineHolder[xPt].line]);
			d3.select("#tooltip3")			
				.text("g upper="+lineHolder[xPt].nair+", g lower="+lineHolder[xPt].gslf)
				.style("opacity",1)
				.style("fill",colors[lineHolder[xPt].line]);				
        	hoverLine
				.attr("cx",x(lineHolder[xPt].nu))
				.attr("cy",y(totalAbs))
				.attr("clip-path","url(#clip)")
				.style("opacity",1)
				.style("stroke",colors[lineHolder[xPt].line]);

				}
        break;

        //case 40: // down
        //break;

        default: return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
});



//$('input[type=text]').bind('keydown', function(e) {
//      if (e.keyCode == 13) {submit_form(e);}});



//update dropdown text
$(".hitran_species").click(function(){
  var selText = $(this).text();
  var selHTML = $(this).html();

  if(selText=="None"){
  		selText="Species";
  		selHTML="Species";}
  
  $(this).parents('.spectable').find(".specspan").html(selHTML);
  $(this).parents('.spectable').find('.specspan').find('.db').show();		  
});

$('.nist_ion').click(function(e) {
        e.stopPropagation(); //This will prevent the event from bubbling up and close the dropdown when you type/click on text boxes.
    });

$('.ionstate').find('.btn').click(function(e)  {
$(this).addClass("active");
$(this).siblings(".btn").removeClass("active");
});

$('.nist_species').click(function(){
  var selText = $(this).text();
  var selHTML = $(this).html();
  ionNum = $(this).parents('.btn-group').find('.btn.active').text();  

	if(selText=="None"){
  		selText="Species";
  		selHTML="Species";
  	}

	if(ionNum=='X' || selText=="Species"){
  		$(this).parents('.spectable').find(".specspan").html(selHTML);
  	}
  	else if(ionNum=='X+'){
  		$(this).parents('.spectable').find(".specspan").html(selHTML+"<sup>+</sup>");
  	}
  	else if(ionNum='X2+'){
  		$(this).parents('.spectable').find(".specspan").html(selHTML+"<sup>2+</sup>");
  		if(selText=="H"){ // catch for Hydrogen 2+
  		$(this).parents('.spectable').find(".specspan").html(selHTML+"<sup>+</sup>");
  		}
  	} 
});



// cookie operations, setting form on page load...
if (typeof $.cookie(cookieName_hitran) === 'undefined'){
	//no cookie
} else {
	//have cookie
	//$.cookie("spectraPlotParameters", [T,P,L,vstart,vend,deltav,xArrayTotal,speciesArrayHTML]);
	cookieArray = $.cookie(cookieName_hitran).split(",");
//[T,P,vstart,vend,Scutoff,speciesArrayHTML]
	$('input[name="T_hitran"]').val(cookieArray[0]); //T
	$('input[name="P_hitran"]').val(cookieArray[1]); //P
	$('input[name="vstart_hitran"]').val(cookieArray[2]) //vstart;
	$('input[name="vend_hitran"]').val(cookieArray[3]);  //vend
	$('input[name="Scutoff"]').val(cookieArray[4]); //Scutoff
			
	
	$('#xspec1_hitran').html(cookieArray[5]);
	$('#specspan1_hitran').html(cookieArray[5]);
	$('input[name="xspecies1_hitran"]').val(cookieArray[8]);
	
	$('#xspec2_hitran').html(cookieArray[6]);
	$('#specspan2_hitran').html(cookieArray[6]);
	$('input[name="xspecies2_hitran"]').val(cookieArray[9]);

	$('#xspec3_hitran').html(cookieArray[7]);
	$('#specspan3_hitran').html(cookieArray[7]);
	$('input[name="xspecies3_hitran"]').val(cookieArray[10]);
}

// cookie operations, setting form...
if (typeof $.cookie(cookieName_nist) === 'undefined'){
	//no cookie
} else {
	//have cookie
	//$.cookie("spectraPlotParameters", [T,P,L,vstart,vend,deltav,xArrayTotal,speciesArrayHTML]);
	cookieArray = $.cookie(cookieName_nist).split(",");
//[T,P,vstart,vend,Scutoff,speciesArrayHTML]
	$('input[name="Telec"]').val(cookieArray[0]); //T
	
	$('input[name="vstart_nist"]').val(cookieArray[1]) //vstart;
	$('input[name="vend_nist"]').val(cookieArray[2]);  //vend
			
	//$('input[name="xspecies1"]').val(cookieArray[6]);
	$('#xspec1_nist').html(cookieArray[3]);
	$('#specspan1_nist').html(cookieArray[3]);
	$('input[name="xspecies1_nist"]').val(cookieArray[6]);
	
	$('#xspec2_nist').html(cookieArray[4]);
	$('#specspan2_nist').html(cookieArray[4]);
	$('input[name="xspecies2_nist"]').val(cookieArray[7]);

	$('#xspec3_nist').html(cookieArray[5]);
	$('#specspan3_nist').html(cookieArray[5]);
	$('input[name="xspecies3_nist"]').val(cookieArray[8]);
}
//console.log(cookieName_nist)
//console.log(cookieName_hitran)


startum2cm();
endum2cm();
$("button#calculate_hitran").click();


});



  
