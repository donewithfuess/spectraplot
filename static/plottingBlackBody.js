$(function() {

var numPlots = 0;
var numLines = 0;	
var firstPlot = 0;
var colors = ["blue","orange","purple","magenta","cyan","black","red","green","yellow","gray"];
//var lineStyles = ["0","2,2","3,1"];
var formatNu2dec  = d3.format("4.3g");
var formatAbs3dec = d3.format("3.3g");
var lineStrings = [];
var margin = {top: 60, right: 30, bottom: 150, left: 90},
    margin2 = {top: 380, right: 30, bottom: 20, left: 90},
    width = 900 - margin.left - margin.right,
    height = 450 - margin.top - margin.bottom/1.5,
    height2 = 450 - margin2.top - margin2.bottom;
    
var ylimStore;



//var x      = d3.scale.pow().exponent(-1).range([0, width]),
//    x2     = d3.scale.pow().exponent(-1).range([0, width]),
//    xtop   = d3.scale.linear().range([0, width]),
var x      = d3.scale.linear().range([0, width]),
    x2     = d3.scale.linear().range([0, width]),
    xtop   = d3.scale.pow().exponent(-1).range([0, width]),

    y      = d3.scale.linear().range([height, 0]),
    y2     = d3.scale.linear().range([height2, 0]),
    yright = d3.scale.linear().range([height, 0]);
    

//function for getting max y in brush window 
var bisect = d3.bisector(function(d) { return d.nu; }).left;


//set up axes and formats
//var xAxis       = d3.svg.axis().scale(x).orient("bottom").ticks(8),
//    xAxis2      = d3.svg.axis().scale(x2).orient("bottom").ticks(8),
//    xAxisTop    = d3.svg.axis().scale(xtop).orient("top").ticks(8),

var xAxis       = d3.svg.axis().scale(x).orient("bottom"),
    xAxis2      = d3.svg.axis().scale(x2).orient("bottom"),
    xAxisTop    = d3.svg.axis().scale(xtop).orient("top"),
    yAxis       = d3.svg.axis().scale(y).orient("left");
    yAxisRight  = d3.svg.axis().scale(y).orient("right");

xAxis.tickFormat(function(e){return d3.round(e,7)});
yAxis.tickFormat(d3.format("3.g"));
yAxisRight.tickFormat("");
xAxis2.tickFormat(d3.format("4.g"));
xAxisTop.tickFormat(function(e){return d3.round(e,3)});
//xAxisTop.tickFormat(d3.format("4.g"));
xtop.domain([10,5]);

var brush = d3.svg.brush()
    .x(x2)
    .on("brush", brushed);

var line = d3.svg.line()
    .x(function(d) { return x(d.nu); })
    .y(function(d) { return y(d.abs); });


var line2 = d3.svg.line()
	.x(function(d) { return x2(d.nu); })
	.y(function(d) { return y2(d.abs); });

var svg = d3.select("#chart").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .attr("id","plot");
    

svg.append("defs").append("clipPath")
    .attr("id", "clip")
    .append("rect")
    .attr("width", width)
    .attr("height", height);
    
svg.append("defs").append("marker")
    .attr("id", "arrowhead")
    .attr("refX",'1.5') /*must be smarter way to calculate shift*/
    .attr("refY",'3.5')
    .attr("markerWidth",'8')
    .attr("markerHeight",'8')
    .attr("orient", "auto")
    .append("path")
    .attr("d", "M0,0 L8,3.5 L0,7")
    .attr("fill","none")
    .attr("stroke","black");
    
//    .attr("stroke","black")
//   .attr("stroke-width","1px"); //this is actual shape for arrowhead    
	
	
var focus = svg.append("g")
    .attr("class", "focus")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

var context = svg.append("g")
    .attr("class", "context")
    .attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");

var legend = svg.append("g")
    .attr("class", "legend");
    
var introGroup = svg.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");;        

context.append("g")
    .attr("class", "x brush")
    .call(brush)
    .selectAll("rect")
    .attr("y", -6)
    .attr("height", height2 + 7);

//append axes    
focus.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis)
    .attr("font-size","13px")
    .attr("font-family", "sans-serif");

var topaxis = focus.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0,0)")
    .call(xAxisTop)
    .attr("font-size","13px")
    .attr("font-family", "sans-serif");

focus.append("g")
    .attr("class", "y axis")
    .call(yAxis)
    .attr("font-size","13px")
    .attr("font-family", "sans-serif"); 
    
var rightaxis = focus.append("g")
    .attr("class", "y axis")
    .attr("transform", "translate("+width+",0)")
    .call(yAxisRight)
    .attr("font-size","13px")
    .attr("font-family", "sans-serif");     
    
context.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height2 + ")")
    .call(xAxis2)
    .attr("font-size","13px")
    .attr("font-family", "sans-serif");
    
//axis titles
context.append("text")
    .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
    .attr("transform", "translate("+ (width/2) +","+(height2+margin2.bottom*2)+")")  // centre below axis
	.text("Wavelength, \u03BCm")
    .attr("font-size", "15px")
    .attr("font-family", "sans-serif");	
    
//y axis title    
focus.append("text")
	.attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
    .attr("transform", "translate("+ (-margin.left/1.5) +","+(height/2)+")rotate(-90)")  // text is drawn off the screen top left, move down and out and rotate
    .text("W/(steradian nm m²)")
    .attr("font-size", "15px")
    .attr("font-family", "sans-serif");  

//wavelength axis title    
focus.append("text")
	.attr("text-anchor","middle")
	.attr("transform","translate("+ (width/2) +", -30)")
	.text("Wavelength, \u03BCm")
    .text("Frequency, cm⁻¹")	
	.attr("font-size", "15px")
	.attr("font-family", "sans-serif");      
    
    
svg.append("text")
		  .attr("id", "tooltip")
		  .attr("x", width)
		  .attr("y", height+height2+50-2)
		  .attr("text-anchor", "right")
		  .attr("font-family", "sans-serif")
		  .attr("font-size", "11px")
		  .attr("font-weight", "bold")
		  .attr("fill", "black");
		
		  
var hoverLine = focus.append("line")
        .attr("class", "hover-line")
		.attr("x1", 1)
		.attr("x2", 1) 
		.attr("y1", height)
		.attr("y2", 1)
		.style("z-index","100"); 
////////////////////////////////		
	
	
// function for plotting	
var submit_form = function(e){

	// clear out demo text/picture on running first simulation
	if(firstPlot==1){clear_plot();}
	
	// for first plot, get rid of first image/group that's been plotted
	if(firstPlot>0){introGroup.attr("visibility","hidden");}

	//disable calculate button
	var btn = $('#calculate');
    btn.button('loading');
    
    var wasChecked = false;

	//initialize number of lines, and see how many species are going to be calculated
	numLines = 0;

	//assemble simulation parameters
	T = $('input[name="T"]').val();
	e = $('input[name="e"]').val();
	vstart = $('input[name="vstart"]').val();
	vend = $('input[name="vend"]').val();
	deltav = (vend-vstart)/10000;
	cook   = $.cookie('_ga');
	
	
	if(firstPlot==0){
		T = 3000;
		e = 1;
		vstart = 2000;
		vend = 100000;}
	
/////// Here's the AJAX call to the python script	
       $.ajax({
		 url:	$SCRIPT_ROOT + '/_calcBB',
		data: {T: T,
			   e: e,
          vstart: vstart,
            vend: vend,
   firstPlotFlag: firstPlot,
            cook: cook},

	 success: function(data) { 
				btn.button('reset');
				if ($('input[name="flipX"]').is(':checked')) {	
					$('input[name="flipX"]').attr('checked',false);
					wasChecked = true;}
				
	 			//for every piece of data returned
	 				Object.keys(data.output).forEach(function(val) {
	 						    data.output[val].forEach(function(d) {
    							d.nu  = +d.nu;
    							d.abs = +d.abs;});

			if (numLines > 0 || numPlots > 0) {		// if you've already plotted a line
			  	newxdomain = d3.extent(data.output[val], function(d) { return d.nu; });
			  	newydomain = 1.05*d3.max(data.output[val], function(d) { return d.abs; });
				//find bounds from new domain and old domain by concatenating them - then taking extent
			  	x.domain(d3.extent(newxdomain.concat(x.domain())));
			 	y.domain(d3.extent(y.domain().concat(newydomain)));}
		  	else {
		  		x.domain(d3.extent(data.output[val], function(d) { return d.nu; }));
			  	y.domain([0, 1.05*d3.max(data.output[val], function(d) { return d.abs; })]);
			  	}
		  // now we're outside of focus bound finding
		  x2.domain(x.domain());
		  y2.domain(y.domain());
		  yright.domain(y.domain());

		  //store the y limits so the view changes back to the original when the autoscaleY checkbox is ticked and unticked
		  ylimStore = y.domain();  

		  //draw the lines
		  focus.append("path")
		      .datum(data.output[val])
		      .attr("class", "line")
		      .attr("d", line)
			  .attr("stroke",colors[numPlots]);

		  context.append("path")
		  		.datum(data.output[val])
		  		.attr("class", "line")
		  		.attr("d", line2)
			    .attr("stroke",colors[numPlots]);
			    
			

        //construct legend
        legend.append("line")
			.attr("x1", width - 150)
          	.attr("y1", 70+(val*20)+(numPlots*60))
			.attr("x2", width - 135)
          	.attr("y2", 80+(val*20)+(numPlots*60))
           	.attr("width", 10)
          	.attr("height", 10)
          	.attr("stroke-width", 2)
            .attr("stroke", colors[numPlots])
            .attr("class","labels");
          		 
	        
	    if(numPlots==0){
	    legend.append("text")
	    	.attr("x", width-20)
	    	.attr("y", height+height2)
	    	.attr("class","labels")
	    	.text("SpectraPlot.com")
	    	.attr("font-family", "sans-serif")
	    	.attr("font-size", 14);}    


		  //update all the axes/lines except wavelength and right-side axis
		  context.select(".x.axis").call(xAxis2);
		  focus.select(".x.axis").call(xAxis);
		  focus.select(".y.axis").call(yAxis);
		  context.selectAll(".line").attr("d",line2);
		  focus.selectAll(".line").attr("d",line);

	      thisLineString = '/T=' + T + ' K/ epsilon=' + e;
		  lineStrings.push(thisLineString);
		  numLines++;  		
		  		}); //loop around all datasets returned from simulator

        legend.append("text")
			.attr("x", width - 135)
        	.attr("y", 70+(numPlots*60)+10)
		    .attr("height",30)
		    .attr("width",100)
		    .attr("font-family", "sans-serif")
			.attr("font-size", "12px")
		    .attr("class","labels")
		    .style("fill", "black")
			.text("T = "+T+" K, \u03B5=" + e);
			
		legend.append("text")
			.attr("id","intPower"+numPlots)
			.attr("class","intpowerIndicator")
			.attr("x", width - 150)
        	.attr("y", 70+(numPlots*60)+25)
        	.attr("font-family", "sans-serif")
			.attr("font-size", "12px")
        	.style("opacity",1e-6);	
			
		legend.append("text")
			.attr("id","power"+numPlots)
			.attr("class","powerIndicator")
			.attr("x", width - 150)
        	.attr("y", 70+(numPlots*60)+40)
        	.attr("font-family", "sans-serif")
			.attr("font-size", "12px")
        	.style("opacity",1e-6);
        

			           
	   focus.append("rect")
		  	.attr("class", "overlay")
      		.attr("width", width)
     		.attr("height", height)
    		.on("mouseout", function() { hoverLine.style("opacity",1e-6);
    									 d3.select("#tooltip").style("opacity",1e-6);
    									 legend.selectAll(".powerIndicator").style("opacity",1e-6);
    									 })

		    .on("mousemove", function(d) { 
				var plotData = focus.select(".line").data()[0];
					if (plotData){
						var x0 = x.invert(d3.mouse(this)[0]);
						var xPt  = bisect(plotData,x0,1);
						
						for (j in focus.selectAll(".line").data()) {
    					lineData = 	focus.selectAll(".line").data()[j];
							legend.select("#power"+j)
								.text("Spectral radiance = "+formatAbs3dec(lineData[xPt].abs)+" W/sr-nm-m²")
								.style("opacity",1);
								}
						

					//add the vertical line text
					d3.select("#tooltip")			
				    .text("\u03BD = "+formatNu2dec(data.output[0][xPt].nu)+" um")
				    .style("opacity",1);

					//move vertical line
			    	mousex = d3.mouse(this)[0];
				    hoverLine
				 	.attr("x1",mousex)
					.attr("x2",mousex)
					.style("opacity",1);
					}  //if plotData is not null
				})
		  		
			numPlots++;
			//on page load, put up the fancy picture
			if(firstPlot==0){

				displayIntroGroup(introGroup,width,height);			
			
				brush.extent([.5, 3]);
				svg.select(".brush").call(brush);
				brushed();
			}
			if(firstPlot>0){
				$.cookie("spectraPlotBlackBody", [T,vstart,vend,e]);
				buildTables(data);
				$(document.body).animate({
		    		'scrollTop':   $('#topofplot').offset().top
					}, 500);	
				}
			
			firstPlot++;
			
			if(numPlots>=4){btn.button('disabled');}
			
			
			//now set lambda x-axis
			lowLambda = 10000/d3.min(x.domain());
			highLambda = 10000/d3.max(x.domain());
			
			xtop.domain([lowLambda, highLambda]);
			topaxis.call(xAxisTop);
			rightaxis.call(yAxisRight);
			
			
			if (wasChecked==true) {	
				
				$('input[name="flipX"]')[0].checked = true;
				wasChecked = false;
				}
			brushed();
		}, //close success

	error: function(jqXHR, textStatus, errorThrown) {
		if(textStatus==="timeout") {
			$('#timeoutDiv').show();    
		    } 
		else{
			$('#alertDiv').show();
			}
		},
//	timeout: 30000
				});  //close ajax call
    		};  //close submit_form
    
    
var clear_plot = function(e) {    
    focus.selectAll(".line").remove();
    context.selectAll(".line").remove();
    numPlots = 0;
    d3.selectAll(".brush").call(brush.clear());
    legend.selectAll(".labels").remove();
    legend.selectAll(".intpowerIndicator").remove();
    lineStrings = [];
    var btn = $('#calculate');
    btn.button('reset');
	introGroup.attr("visibility","hidden");
    $('.alert-dismissible').hide();

}


function brushed() {
	// get domain


		x.domain(brush.empty() ? x2.domain() : brush.extent());

		//concatenated data to find maximum y value
	var data = [];
	for (i in focus.selectAll(".line").data()) {
    	data = focus.selectAll(".line").data()[i];
    	var dataSum =0 ;
    	var dataFiltered = data.filter(function(d, i) {
    	if ((d.nu >= d3.min(x.domain())) && (d.nu <= d3.max(x.domain())))  {
    		dataSum += (d.abs*10/1000); 
      		//return d.abs;
    		}
		})
		legend.select("#intPower"+i)
				.text("Integrated radiance = "+formatAbs3dec(dataSum)+" kW/sr-m²")
				.style("opacity",1);
		}
			
		
	// if y zoom box checked, autozoom y
	if ($('input[name="autoscaleY"]').is(':checked')) {	
	//concatenated data to find maximum y value
		var data = [];
		for (i in focus.selectAll(".line").data()) {
    		data = data.concat(focus.selectAll(".line").data()[i]);
	    }
    	var dataFiltered = data.filter(function(d, i) {
    	if ((d.nu >= d3.min(x.domain())) && (d.nu <= d3.max(x.domain())))  {
      		return d.abs;
    		}
		})
	y.domain([0, 1.05*d3.max(dataFiltered.map(function(d) { return d.abs; }))]);
	}
	else { 
		if($('input[name="fixY"]').is(':checked')) {
  		y.domain([0,$('input[name="fixYval"]').val()]);
		}
  		else{y.domain(ylimStore);}
  		}
  		
  		//put new ticks for cm-1 that correspond to um
  		var newticks = x.ticks(5);
  		for (var i = 0;i<newticks.length;i++){
  			newticks[i] = 10000/newticks[i];
  			}
		xAxisTop.tickValues(newticks);


  focus.selectAll(".line").attr("d", line).style("clip-path","url(#clip)");
  focus.select(".x.axis").call(xAxis);
  focus.select(".y.axis").call(yAxis);
  rightaxis.call(yAxisRight);
  lowLambda = 10000/(x.domain()[0]);
  highLambda = 10000/(x.domain()[1]);
			
  xtop.domain([lowLambda, highLambda]);
  topaxis.call(xAxisTop);
}

function flipXclick() {

	x2.domain([x2.domain()[1], x2.domain()[0]]);	
	context.select(".x.axis").call(xAxis2);
	context.selectAll(".line").attr("d",line2);
	brushed();
	}

function autoscaleYclick() {
	$('input[name="fixY"]').attr('checked',false);
	brushed();
	}
	
function fixYscale() {
	$('input[name="autoscaleY"]').attr('checked',false);
	brushed();
	}	

function cm2um() {
	var cm = $('input[name="cmCalc"]').val();
	var um = 10000/cm;	
	$('input[name="umCalc"]').val(d3.round(um,5));
	}

function um2cm() {
	var um = $('input[name="umCalc"]').val();
	var cm = 10000/um;
	$('input[name="cmCalc"]').val(d3.round(cm,3));		
	}
	
function startcm2um() {
	var cm = $('input[name="lambdastart"]').val();
	var um = 10000/cm;	
	$('input[name="vstart"]').val(d3.round(um,5));
	}	

function endcm2um() {
	var cm = $('input[name="lambdaend"]').val();
	var um = 10000/cm;	
	$('input[name="vend"]').val(d3.round(um,5));
	}	
	
function startum2cm() {
	var cm = $('input[name="vstart"]').val();
	var um = 10000/cm;	
	$('input[name="lambdastart"]').val(d3.round(um,5));
	}	

function endum2cm() {
	var cm = $('input[name="vend"]').val();
	var um = 10000/cm;	
	$('input[name="lambdaend"]').val(d3.round(um,5));
	}		

function save_data() {

	lineobj= {};
		focus.selectAll(".line").data().forEach(function(infoArray, index){
		lineobj['line'+index]=focus.selectAll(".line").data()[index];
	});
	var url = $SCRIPT_ROOT + '/_saveCSV';
	
	// hide the div, write the form
	$('#downloaddiv').hide()
    	.html('<form id="exportform" action="' + url + '" method="post">'
        + '<textarea name="data">' + JSON.stringify(lineobj) +'</textarea>'
        + '<textarea name="conditions">' + lineStrings +'</textarea>'
        + '</form>');
	// submit the form
	$('#exportform').submit();
};

$('#flipX').bind('change', flipXclick);
$('#autoscaleY').bind('click', autoscaleYclick);
$('#fixY').bind('click', fixYscale);
$('#fixYval').bind('change', fixYscale);
$('button#calculate').bind('click', submit_form);
$('button#clear').bind('click', clear_plot);
$('button#data').bind('click', save_data);
$('input[name=T]').focus();

$('input#cmCalc').bind('keyup',cm2um);
$('input#umCalc').bind('keyup',um2cm); 

$('input[name=lambdastart]').bind('keyup',startcm2um);
$('input[name=lambdaend]').bind('keyup',endcm2um);

$('input[name=vstart]').bind('keyup',startum2cm);
$('input[name=vend]').bind('keyup',endum2cm);

d3.select("#save_as_png").on("click", function(){
    saveSvgAsPng(document.getElementById("plot"), "spectraplot.png",{scale: 2});
    });



//update dropdown text
$(".dropdown-menu li a").click(function(){
  var selText = $(this).text();
  var selHTML = $(this).html();

  if(selText=="None"){
  		selText="Species";
  		selHTML="Species";}
  
  $(this).parents('.btn-group').find('.specspan').html(selHTML);
  speciesNum = $(this).parents('.btn-group').find('.dropdown-toggle').attr('id');
  if(speciesNum=='spec1'){$('#xspec1').html(selHTML);
			  $('#xspec1').find('#db').show();}
  if(speciesNum=='spec2'){$('#xspec2').html(selHTML);
			  $('#xspec2').find('#db').show();}
  if(speciesNum=='spec3'){$('#xspec3').html(selHTML);
			  $('#xspec3').find('#db').show();}
  $(this).parents('.btn-group').find('.dropdown-toggle').find('#db').show();
//console.log($(this).parents('.dropdown-toggle').attr('id')+" is "+selText);
//console.log($(this).parents('.btn-group').find('.dropdown-toggle').attr('id'));

});


// cookie operations, setting form...
if (typeof $.cookie('spectraPlotBlackBody') === 'undefined'){
	//no cookie
} else {
	//have cookie
	//$.cookie("spectraPlotParameters", [T,P,L,vstart,vend,deltav,xArrayTotal,speciesArrayHTML]);
	cookieArray = $.cookie('spectraPlotBlackBody').split(",");

	$('input[name="T"]').val(cookieArray[0]); //T
	$('input[name="vstart"]').val(cookieArray[1]) //vstart;
	$('input[name="vend"]').val(cookieArray[2]);  //vend
	$('input[name="e"]').val(cookieArray[3]);//e
	}


startum2cm();
endum2cm();
submit_form();


});



  
