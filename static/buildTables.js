var trackOutboundLink = function(url,label) {
   ga('send', 'event', 'outbound', 'click', label, {'hitCallback':
     function () {
     window.open(url);
     }
   });
}

function trackTableClick(label) {
	var vstart = $('input[name="vstart"]').val();
	var vend = $('input[name="vend"]').val();
	var myip = '0.0.0.0';
	label+= ','+vstart+','+vend+','+window.location.pathname+','+myip;
   ga('send', 'event', 'tableClick', 'click', label);
   
}
	
function buildTables(data){
	var myip = '0.0.0.0';
		$('#laserTable').empty();
			var content  = '<table style="table-layout: fixed;background-color:white;" id="laserTableSort">';
				content	+= '<thead><tr><th align="center" width="8%"><span/>Vendor</th>';
				content += '<th align="center" width="8%"><span/>Type</th>';
				content += '<th align="center" width="8%"><span/>Center Wavenumber (cm<sup>-1</sup>)</th>';
				content += '<th align="center" width="8%"><span/>&Delta;&nu;<br> (cm<sup>-1</sup>)</th>';
				content += '<th align="center" width="8%"><span/>Power (mW)</th>';
				content += '<th align="center" width="8%"><span/>Linewidth (MHz)</th></tr></thead><tbody>';
			data.lasers.forEach(function(d) {
				var vendor = d[0];
				var url    = d[8];
				var type   = d[3];					
				var cwave  = Math.round(d[9]*10)/10;
				if(isNaN(cwave)){cwave='Custom'};
				if(vendor=='Nanoplus'){cwave='Continuous coverage'};
				var power  = d[4];
				var lwidth  = d[5];
				console.log(lwidth);
				if(lwidth==0){lwidth='n/a'}; 
				var tuning  = d[7];
				content += '<tr><td align="center" width="8%"><a href="'+url+'" target="_blank" onclick="trackOutboundLink(\''+url+'\',\'laser,'+vendor+','+cwave+','+myip+'\'); return false;">' + vendor + '</a></td>';
				content += '<td align="center" width="8%">'+type+'</td>';
				content += '<td align="center" width="8%">' + cwave + '</td>';
				content += '<td align="center" width="8%">'+tuning+'</td>';
				content += '<td align="center" width="8%">'+power+'</td>';
				content += '<td align="center" width="8%">' + lwidth + '</td></tr>';
				})
			content += '</tbody></table>';
			$('#laserTable').append(content);
			$("#laserTableSort").tablesorter({
				theme: 'default',
				stringTo: 'max',
				widgets: ['stickyHeaders'],
				widgetOptions: { stickyHeaders_attachTo : '.lasersWrapper'},
				sortList: [[2,0]]
				});
				
		$('#detectorTable').empty();
		var content  = '<table style="table-layout: fixed;background-color:white;" id="detectorTableSort">';
			content 	+= '<thead><tr><th align="center" width="20%"><span/>Vendor</th>';
			content 	+= '<th align="center" width="20%"><span/>Material</th>';
			content 	+= '<th align="center" width="20%"><span/>Control</th>';
			content 	+= '<th align="center" width="20%"><span/>D<sup>*</sup> Range</th>';
			content 	+= '<th align="center"><span/>&Delta;&nu; (&mu;m)</th></tr>';
			content		+= '</thead><tbody>';
			data.detectors.forEach(function(d) {
				var vendor    = d[0];
				var url       = d[9];
				var material  = d[3];
				var control   = d[4];
				var dstar     = d[5];
				var wavehigh   = Math.round(10000/d[1]*10)/10;
				var wavelow  = Math.round(10000/d[2]*10)/10;
				var areaRange = d[6];
				content += '<tr><td width="20%" align="center"><a href="'+url+'" target="_blank" onclick="trackOutboundLink(\''+url+'\',\'detector,'+vendor+','+material+','+myip+'\'); return false;">' + vendor + '</a></td>';
				content += '<td width="20%" align="center">' + material + '</td>';
				content += '<td width="20%" align="center">' + control + '</td>';
				content += '<td align="center" width="20%">' + dstar + '</td>';
				content += '<td align="center">' + wavelow + ' to ' + wavehigh +'</td></tr>';
				})
			content += '</tbody></table>';
			$('#detectorTable').append(content);
			$("#detectorTableSort").tablesorter({
				theme: 'default',
				widgets: ['stickyHeaders'],
				widgetOptions: {
      			stickyHeaders_attachTo : '.detectorsWrapper'},
      			sortList: [[3,0]]
				});
				

		$('#opticsTable').empty();
			var content  = '<table class="table" style="table-layout: fixed; background-color:white;" id="opticsTableSort">';
				content		+= '<thead><tr><th align="center" width="15%"><span/>Material</th>';
				content 	+= '<th align="center" width="25%"><span/>Transmission Range (&mu;m)</th>';
				content 	+= '<th align="center" width="20%"><span/>Index of Refraction</th>';
				content 	+= '<th align="center" width="20%"><span/>Modulus of Rupture</th>';
				content 	+= '<th align="center" width="20%"><span/>Melting Temp (<sup>&#9675;</sup>C)</th></tr>';
				content 	+= '</thead><tbody>';
				data.optics.forEach(function(d) {
					var material  = d[0];
					var lowLim    = d[6];
					var highLim   = d[7];
					var z         = d[9];
					var rupture   = d[3];
					var melt      = d[5];
					
					var vendors   = d[10];
					vendorsArray  = vendors.split(";");
					var urls  	  = d[11];
					urlsArray	  = urls.split(";");
					var popoverContent = '';
					vendorsArray.forEach(function(d,i){
						popoverContent += '<a href=\''+urlsArray[i]+'\' target=\'_blank\' onclick=&quot;trackOutboundLink(\''+urlsArray[i]+'\',\'optics,'+d+','+material+'\'); return false;&quot;>'+d+'</a><br>';
						//console.log("onclick=\'trackOutboundLink(\''"+urlsArray[i]+"'\',\''"+d+"'\'); return false;\'");
						//popoverContent += '<a href="'+urlsArray[i]+'" target="_blank\" onclick="trackOutboundLink(\''+urlsArray[i]+'\',\''+d+','+material+'\'); return false;">'+d+'</a><br>';
					}); 
					content += '<tr><td width="15%" align="center"><a href="javascript:;" data-toggle="popover" tabindex="0" data-trigger="focus" title="Vendors" data-content="'+popoverContent+'">'+ material + '</a></td>';
					content += '<td align="center" width="25%">' + lowLim + ' to '+ highLim +'</td>';
					content += '<td width="20%" align="center">'+z+'</td>';
					content += '<td width="20%" align="center">'+rupture+'</td>';
					content += '<td width="20%" align="center">'+melt+'</td></tr>';
					})
				content += '</tbody></table>';
				$('#opticsTable').append(content);
				$('[data-toggle="popover"]').popover({
        			placement : 'top',html: true
    				});
    			$("#opticsTableSort").tablesorter({
					theme: 'default',
					widgets: ['stickyHeaders'],
					widgetOptions: {
      				stickyHeaders_attachTo : '.opticsWrapper'},
      				sortList: [[2,0]]
				});	

		$('#filtersTable').empty();
		var content  = '<table style="table-layout: fixed;background-color:white;" id="filtersTableSort">';
			content	+= '<thead><tr><th align="center" width="20%"><span/>Vendor</th>';
			content	+= '<th align="center" width="20%"><span/>Pass band (nm)</th>';
			content	+= '<th align="center" width="20%"><span/>Size (mm)</th>';
			content	+= '<th align="center" width="20%"><span/>Substrate</th>';
			content	+= '<th align="center" width="20%"><span/>Peak Transmission (%)</th></tr>';
			content	+= '</thead><tbody>';
				data.filters.forEach(function(d) {
					var vendor    = d[0];
					var url       = d[9];
					var cwl  	  = d[2];
					var trans     = d[6];
					var filterSize= d[7];
					var hpb       = d[3]/2;
					var substrate = d[8];

					content += '<tr><td width="20%" align="center"><a href="'+url+'" target="_blank" onclick="trackOutboundLink(\''+url+'\',\'filter,'+vendor+','+cwl+','+hpb+','+myip+'\'); return false;">' + vendor + '</a></td>';
					content += '<td width="20%" align="center">' + cwl + '±'+hpb+'</td>';
					content += '<td width="20%" align="center">' + filterSize + '</td>';
					content += '<td width="20%" align="center">' + substrate + '</td>';
					content += '<td align="center" width="20%">' + trans + '</td></tr>';
					})
				content += '</tbody></table>';
				$('#filtersTable').append(content);
				$("#filtersTableSort").tablesorter({
					theme: 'default',
					widgets: ['stickyHeaders'],
					widgetOptions: {
      				stickyHeaders_attachTo : '.filtersWrapper'},
      				sortList: [[1,0]]
				});	



		$('#fibersTable').empty();
		var content  = '<table style="table-layout: fixed;background-color:white;" id="fibersTableSort">';
			content	+= '<thead><tr><th align="center" width="20%"><span/>Vendor</th>';
			content	+= '<th align="center" width="15%"><span/>Material</th>';
			content	+= '<th align="center" width="15%"><span/>Mode</th>';
			content	+= '<th align="center" width="30%"><span/>Core Diameter (&mu;m)</th>';
			content	+= '<th align="center" width="20%"><span/>Wavelength range (&mu;m)</th></tr>';
			content += '</thead><tbody>';
			data.fibers.forEach(function(d) {
					var vendor    = d[0];
					var url       = d[8];
					var material  = d[1];
					var mode      = d[4];
					var diameter  = d[5];
					var waveLow   = d[9];
					var waveHigh  = d[10];
					content += '<tr><td width="20%" align="center"><a href="'+url+'" target="_blank" onclick="trackOutboundLink(\''+url+'\',\'fiber,'+vendor+','+material+','+mode+','+myip+'\'); return false;">' + vendor + '</a></td>';
					content += '<td width="15%" align="center">' +material+'</td>';
					content += '<td width="15%" align="center">' + mode + '</td>';
					content += '<td width="30%" align="center">' + diameter + '</td>';
					content += '<td align="center" width="20%">' + waveLow + ' to ' +waveHigh+ '</td></tr>';
					})
				content += '</tbody></table>';
				$('#fibersTable').append(content);
				$("#fibersTableSort").tablesorter({
					theme: 'default',
					widgets: ['stickyHeaders'],
					widgetOptions: {
      				stickyHeaders_attachTo : '.fibersWrapper'},
      				sortList: [[3,0]]      				
				});
				
		$('#mirrorsTable').empty();
		var content  = '<table style="table-layout: fixed;background-color:white;" id="mirrorsTableSort">';
			content	+= '<thead><tr><th align="center" width="15%"><span/>Vendor</th>';
			content	+= '<th align="center" width="20%"><span/>Reflectivity (%)</th>';
			content	+= '<th align="center" width="20%"><span/>Wavelength range (nm)</th>';
			content	+= '<th align="center" width="25%"><span/>Radius of curvature (m)</th>';
			content	+= '<th align="center" width="20%"><span/>Diameter (in)</th></tr>';
			content += '</thead><tbody>';
			data.mirrors.forEach(function(d) {
					var vendor    		= d[0];
					var url       		= d[9];
					var reflectivity	= d[6];
					var cwl      		= d[4];
					var bp			  	= d[5];
					var diameter  		= d[8];
					var roc   			= d[7];
					content += '<tr><td width="15%" align="center"><a href="'+url+'" target="_blank" onclick="trackOutboundLink(\''+url+'\',\'mirror,'+vendor+','+cwl+','+bp+','+myip+'\'); return false;">' + vendor + '</a></td>';
					content += '<td width="20%" align="center">' +reflectivity+'</td>';
					content += '<td width="20%" align="center">' + cwl + '±' + bp + '</td>';
					content += '<td width="25%" align="center">' + roc + '</td>';
					content += '<td width="20%" align="center">' + diameter + '</td></tr>';
					})
				content += '</tbody></table>';
				$('#mirrorsTable').append(content);
				$("#mirrorsTableSort").tablesorter({
					theme: 'default',
					widgets: ['stickyHeaders'],
					widgetOptions: {
      				stickyHeaders_attachTo : '.mirrorsWrapper'},
      				sortList: [[2,0]]      				
				});				
				
				
					
	return;
}
							