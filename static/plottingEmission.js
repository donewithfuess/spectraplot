$(function() {


var numPlots = 0,
	numLines = 0,	
 	totalLines =0,
 	firstPlot = 0,
	linesPer = [0,0,0],
	deltavarr = [0,0,0],  //delta v array to use in calculation of integrated emission
	colors = ["blue","orange","purple","black"],
	lineStyles = ["none","2,2","3,1"],
	formatNu2dec  = d3.format("4.6g"),
	formatAbs3dec = d3.format("3.2g"),
	lineStrings = [],
	margin = {top: 60, right: 30, bottom: 150, left: 90},
    margin2 = {top: 380, right: 30, bottom: 20, left: 90},
    width = 900 - margin.left - margin.right,
    height = 450 - margin.top - margin.bottom/1.5,
    height2 = 450 - margin2.top - margin2.bottom,
    ylimStore;

var btn_hitran = $('#calculate_hitran');
var btn_nist = $('#calculate_nist');

var x      = d3.scale.linear().range([0, width]),
    x2     = d3.scale.linear().range([0, width]),
    xtop   = d3.scale.pow().exponent(-1).range([0, width]),
    y      = d3.scale.linear().range([height, 0]),
    y2     = d3.scale.linear().range([height2, 0]),
    yright = d3.scale.linear().range([height, 0]);
//function for getting max y in brush window 
var bisect = d3.bisector(function(d) { return d.nu; }).left;


//set up axes and formats
var xAxis       = d3.svg.axis().scale(x).orient("bottom"),
    xAxis2      = d3.svg.axis().scale(x2).orient("bottom"),
    xAxisTop    = d3.svg.axis().scale(xtop).orient("top"),
    yAxis       = d3.svg.axis().scale(y).orient("left");
    yAxisRight  = d3.svg.axis().scale(y).orient("right");

xAxis.tickFormat(function(e){return d3.round(e,7)});
yAxis.tickFormat(d3.format("3.2e"));
yAxisRight.tickFormat("");
xAxis2.tickFormat(d3.format("4.g"));
xAxisTop.tickFormat(function(e){return d3.round(e,7)});
xtop.domain([10,5]);
var brush = d3.svg.brush()
    .x(x2)
    .on("brush", brushed);


var line = d3.svg.line()
    .x(function(d) { return x(d.nu); })
    .y(function(d) { return y(d.abs); });


var line2 = d3.svg.line()
	.x(function(d) { return x2(d.nu); })
	.y(function(d) { return y2(d.abs); });

var svg = d3.select("#chart").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .attr("id","plot");
    

svg.append("defs").append("clipPath")
    .attr("id", "clip")
    .append("rect")
    .attr("width", width)
    .attr("height", height);
    
svg.append("defs").append("marker")
    .attr("id", "arrowhead")
    .attr("refX",'1.5') /*must be smarter way to calculate shift*/
    .attr("refY",'3.5')
    .attr("markerWidth",'8')
    .attr("markerHeight",'8')
    .attr("orient", "auto")
    .append("path")
    .attr("d", "M0,0 L8,3.5 L0,7")
    .attr("fill","none")
    .attr("stroke","black");
    
//    .attr("stroke","black")
//   .attr("stroke-width","1px"); //this is actual shape for arrowhead    
	
	
var focus = svg.append("g")
    .attr("class", "focus")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

var context = svg.append("g")
    .attr("class", "context")
    .attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");

var legend = svg.append("g")
    .attr("class", "legend");
    
var introGroup = svg.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");;        

context.append("g")
    .attr("class", "x brush")
    .call(brush)
    .selectAll("rect")
    .attr("y", -6)
    .attr("height", height2 + 7);

//append axes    
focus.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis)
    .attr("font-size","13px")
    .attr("font-family", "sans-serif");

var topaxis = focus.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0,0)")
    .call(xAxisTop)
    .attr("font-size","13px")
    .attr("font-family", "sans-serif");

focus.append("g")
    .attr("class", "y axis")
    .call(yAxis)
    .attr("font-size","13px")
    .attr("font-family", "sans-serif"); 
    
var rightaxis = focus.append("g")
    .attr("class", "y axis")
    .attr("transform", "translate("+width+",0)")
    .call(yAxisRight);     
    
context.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height2 + ")")
    .call(xAxis2)
    .attr("font-size","13px")
    .attr("font-family", "sans-serif");
    
//axis titles
context.append("text")
    .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
    .attr("transform", "translate("+ (width/2) +","+(height2+margin2.bottom*2)+")")  // centre below axis
    .text("Frequency, cm⁻¹")
    .attr("font-family", "sans-serif")
    .attr("font-size", "15px");	
    
//y axis title    
focus.append("text")
	.attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
    .attr("transform", "translate("+ (-margin.left/1.15) +","+(height/2)+")rotate(-90)")  // text is drawn off the screen top left, move down and out and rotate
    .text("Emission, \u03BCW/(cm⁻¹-cm²-sr)")
    .attr("font-family", "sans-serif")
    .attr("font-size", "15px");  

//wavelength axis title    
focus.append("text")
	.attr("text-anchor","middle")
	.attr("transform","translate("+ (width/2) +", -30)")
	.text("Wavelength, \u03BCm")
	.attr("font-family", "sans-serif")
	.attr("font-size", "15px");      
    
    
svg.append("text")
		  .attr("id", "tooltip")
		  .attr("x", width)
		  .attr("y", height+height2+50-2)
		  .attr("text-anchor", "right")
		  .attr("font-family", "sans-serif")
		  .attr("font-size", "11px")
		  .attr("font-weight", "bold")
		  .attr("fill", "black");
		
		  
var hoverLine = focus.append("line")
        .attr("class", "hover-line")
		.attr("x1", 1)
		.attr("x2", 1) 
		.attr("y1", height)
		.attr("y2", 1)
		.style("z-index","100"); 
////////////////////////////////		
	
	
// function for plotting	
var submit_form = function(e){
	var simType = e.data.simType;
	
	// clear out demo text/picture on running first simulation
	if(firstPlot==1){clear_plot();}
	
	// for first plot, get rid of first image/group that's been plotted
	if(firstPlot>0){introGroup.attr("visibility","hidden");}

	//disable calculate button
	
    btn_hitran.button('loading');
    btn_nist.button('loading');

	//initialize number of lines, and see how many species are going to be calculated
	numLines = 0;
	if ($("#spec1_"+simType).find('.specspan').text()=='Species') {
		species1 = null;
		species1HTML = "Species";
		xabs1    = null;}
	else { 
		species1 = $("#spec1_"+simType).find('.specspan').text();
		species1HTML = $("#spec1_"+simType).find('.specspan').html();
		xabs1    = $('input[name="xspecies1_'+simType+'"]').val();}
		
	if ($("#spec2_"+simType).find('.specspan').text()=='Species') {
		species2 = null;
		species2HTML = "Species";		
		xabs2    = null;}
	else { 
		species2 = $("#spec2_"+simType).find('.specspan').text();
		species2HTML = $("#spec2_"+simType).find('.specspan').html();		
		xabs2    = $('input[name="xspecies2_'+simType+'"]').val();}	

	if ($("#spec3_"+simType).find('.specspan').text()=='Species') {
		species3 = null;
		species3HTML = "Species";		
		xabs3    = null;}
	else { 
		species3 = $("#spec3_"+simType).find('.specspan').text();
		species3HTML = $("#spec3_"+simType).find('.specspan').html();		
		xabs3    = $('input[name="xspecies3_'+simType+'"]').val();}

	//assemble simulation parameters				   
	speciesArray = [species1,species2,species3];
	speciesArrayHTML = [species1HTML,species2HTML,species3HTML];
	speciesArray = speciesArray.filter(function(n){ return n != undefined });
	//console.log(speciesArray);
	xArray = [xabs1,xabs2,xabs3];
	xArrayTotal = xArray;
	xArray = xArray.filter(function(n){ return n != undefined });
	
	var molefracsTooBig = 0;
	xArray.forEach(function(d){
		if (d>1) { 
			$('#molefracDiv').show();
			molefracsTooBig = 1;
			} 
		});
	
	if (molefracsTooBig == 1) {return;}
		
		
	T = $('input[name="T_'+simType+'"]').val();
	P = $('input[name="P_'+simType+'"]').val();
	L = $('input[name="L_'+simType+'"]').val();
	vstart 	= $('input[name="vstart_'+simType+'"]').val();
	vend 	= $('input[name="vend_'+simType+'"]').val();
	deltav 	= $('input[name="deltav_'+simType+'"]').val();
	gammaTo = $('input[name="gammaTo"]').val();
	n = $('input[name="n"]').val();
	Telec = $('input[name="Telec_nist"]').val();
	cook   	= $.cookie('_ga');
	
	
	var toobig = 0;
	
		if ((vend-vstart>100 || vstart-vend>100)) { 
			$('#toobigAbsEmisDiv').show();
			toobig = 1;
			} 
	
	if (toobig == 1) {return;}

	//deprecated by logic above
	//if ((vstart-vend>100) || (vend-vstart)>100) {
//		$('#simrangeDiv').show();
//		}
		
	if ((vstart > 10000 || vend > 10000) && simType=="hitran") {
		$('#notinfraredDiv').show();
	}		

	console.log(speciesArray.indexOf("N2"));
	if (speciesArray.indexOf("N2")>=0 && L < 100000){
		$('#n2hitran').show();
	}
	var badspecies = 0;
		if ((vend-vstart>1000 || vstart-vend>1000) && speciesArray.indexOf("CO2 HITEMP")>=0) { 
			$('#badspecies').show();
		}
	if (badspecies == 1) {return;}
		if ((vend-vstart>1000 || vstart-vend>1000) && speciesArray.indexOf("H2O HITEMP")>=0) { 
			$('#badspecies').show();
		}
	if (badspecies == 1) {return;}

	if(firstPlot==0){
		T = 1300;
		P = 1;
		L = 10;
		vstart = 1060;
		vend = 1065;
		deltav = 0.002;
		xArray=[.01];
		speciesArray=['CO2'];}
	
/////// Here's the AJAX call to the python script	
       $.ajax({
		 url:	$SCRIPT_ROOT + '/_calcEmission',
		data: {T: T,
			   P: P,
			   L: L,
          vstart: vstart,
            vend: vend,
          deltav: deltav,
         species: speciesArray,
         gammaTo: gammaTo,
         	   n: n,
           Telec: Telec,
       molefracs: xArray,
   firstPlotFlag: firstPlot,
            cook: cook,
            simType: simType},

	 success: function(data) { //console.log(data.output);

	 	if (simType=='nist'){
	 	EinAarrayCount = 0 
	 	EinArray = []
	 	Object.keys(data.EinsteinAFlag).forEach(function(val) {
	 		if (data.EinsteinAFlag[val]==1){
	 			
	 			EinAarrayCount++; 
	 			EinArray.push(speciesArray[val]);
	 		}
	 	});
	 	if (EinAarrayCount>0){
	 			if (EinAarrayCount>1){
	 				EinArray.splice(EinAarrayCount-1,0,'and')
	 			}			
	 		$('#alertSpec').html(EinArray.join(' '));
	 		$('#einsteinA').show();
	 	}
	 }

	 			if(+data.deltavcalc!=deltav){
					$('#reduceDiv').show();
					$('input[name="deltav_'+simType+'"]').val(d3.round(+data.deltavcalc,5));
					deltav = +data.deltavcalc;
	 			} else {
	 				btn_hitran.button('reset');
	 				btn_nist.button('reset');
	 			}


	 			//for every piece of data returned
	 				Object.keys(data.output).forEach(function(val) {
	 						    data.output[val].forEach(function(d) {
    							d.nu  = +d.nu;
    							d.abs = +d.abs;});

			if (numLines > 0 || numPlots > 0) {		// if you've already plotted a line
			  	newxdomain = d3.extent(data.output[val], function(d) { return d.nu; });
			  	newydomain = 1.05*d3.max(data.output[val], function(d) { return d.abs; });
				//find bounds from new domain and old domain by concatenating them - then taking extent
			  	x.domain(d3.extent(newxdomain.concat(x.domain())));
			 	y.domain(d3.extent(y.domain().concat(newydomain)));}
		  	else {
		  		x.domain(d3.extent(data.output[val], function(d) { return d.nu; }));
			  	y.domain([0, 1.05*d3.max(data.output[val], function(d) { return d.abs; })]);
			  	}
		  // now we're outside of focus bound finding
		  x2.domain(x.domain());
		  y2.domain(y.domain());
		  yright.domain(y.domain());

		  //store the y limits so the view changes back to the original when the autoscaleY checkbox is ticked and unticked
		  ylimStore = y.domain();  

		  //draw the lines
		  
			focus.append("path")
				.datum(data.output[val])
		      	.attr("class", "line")
		      	.attr("d", line)
		      	.attr("stroke-dasharray",lineStyles[numPlots])
			  	.attr("stroke",colors[numLines]);
		  	context.append("path")
		  		.datum(data.output[val])
		  		.attr("class", "line")
		  		.attr("d", line2)
		  		.attr("stroke-dasharray",lineStyles[numPlots])
			    .attr("stroke",colors[numLines]);
			

        //construct legend
        legend.append("line")
			.attr("x1", width - 200)
          	.attr("y1", 90+(val*22)+(numPlots*80)-6)
			.attr("x2", width - 185)
          	.attr("y2", 100+(val*22)+(numPlots*80)-6)
           	.attr("width", 10)
          	.attr("height", 10)
          	.attr("stroke-width", 2)
            .attr("stroke", colors[val])
            .attr("stroke-dasharray",lineStyles[numPlots])
            .attr("class","labels");
          		 
        legend.append("text")
          	.attr("x", width - 175)
          	.attr("y", 90+(val*22)+10+(numPlots*80)-6)
          	.attr("height",30)
          	.attr("width",100)
          	.style("fill", colors[val])
          	.attr("font-size", "12px")
          	.attr("font-family", "sans-serif")
          	.attr("class","labels")
	        .text(speciesArray[val]+': \u03C7 = '+xArray[val]);

	     legendNum = +val+numPlots*3;
	     legend.append("text")
			.attr("id","emission"+legendNum)
			.attr("class","emissionIndicator")
			.attr("x", width - 80)
          	.attr("y", 90+(val*22)+10+(numPlots*80)+3)
        	.attr("font-family", "sans-serif")
			.attr("font-size", "10px")
        	.style("opacity",1e-6);	

	     legend.append("text")
			.attr("id","emissionInt"+legendNum)
			.attr("class","emissionIntIndicator")
			.attr("x", width-80)
          	.attr("y", 90+(val*22)+10+(numPlots*80)-7)
        	.attr("font-family", "sans-serif")
			.attr("font-size", "10px")
        	.style("opacity",1e-6);	        	
	        
	    if(numPlots==0 && val < 1){
	    legend.append("text")
	    	.attr("x", width-20)
	    	.attr("y", height+height2)
	    	.attr("class","labels")
	    	.attr("font-family", "sans-serif")
	    	.attr("font-size", "14px")
	    	.text("SpectraPlot.com");}


		  //update all the axes/lines except wavelength and right-side axis
		  context.select(".x.axis").call(xAxis2);
		  focus.select(".x.axis").call(xAxis);
		  focus.select(".y.axis").call(yAxis);
		  context.selectAll(".line").attr("d",line2);
		  focus.selectAll(".line").attr("d",line);

	      thisLineString = speciesArray[val] + '/x=' + xArray[val] + '/T=' + T + 'K/P=' + P + 'atm/L=' + L + 'cm';
		  lineStrings.push(thisLineString);
		  numLines++;  	
		  linesPer[numPlots]++;  	
		  		}); //loop around all datasets returned from simulator
		
		deltavarr[numPlots]= +deltav;
        legend.append("text")
			.attr("x", width - 210)
        	.attr("y", 70+(numPlots*80)+10)
		    .attr("height",30)
		    .attr("width",100)
		    .attr("class","labels")
		    .attr("font-size", "12px")
		    .attr("font-family", "sans-serif")
		    .style("fill", "black")
			.text("T = "+T+"K, P = "+P+" atm, L = "+L+"cm");

		//legend.append("text")
		//	.attr("id","emissionTot"+numPlots)
		//	.attr("class","emissionIndicator")
		//	.attr("x", width - 20)
        //	.attr("y", 70+(numPlots*80)+10)
        //	.attr("font-family", "sans-serif")
		//	.attr("font-size", "12px")
        //	.style("opacity",1e-6);		
		
		
	   focus.append("rect")
		  	.attr("class", "overlay")
      		.attr("width", width)
     		.attr("height", height)
    		.on("mouseout", function() { hoverLine.style("opacity",1e-6);
    									 d3.select("#tooltip").style("opacity",1e-6);
    									 d3.selectAll(".emissionIndicator").style("opacity",1e-6); })

		    .on("mousemove", function(d) { 
				var plotData = focus.select(".line").data()[0];
					if (plotData){
						var x0 = x.invert(d3.mouse(this)[0]);
						var xPt  = bisect(plotData,x0,1);
						
						var lineCount = 0;

						for (i=0;i<3;i++){  //for each plot
							var num2count = linesPer[i];
							var totalEmi = 0;
							for (j=0;j<num2count;j++){
								lineData = 	focus.selectAll(".line").data()[lineCount];
								
								lineCount++;
								//totalEmi+=lineData[xPt].abs;

								legend.select("#emission"+(+j+i*3))
								.text("\u03B5 = "+formatAbs3dec(lineData[xPt].abs)+"\u03BCW/(cm⁻¹-cm²-sr)")
								.style("opacity",1)
								.style("fill", colors[+j]);
							}
							//legend.select("#emissionTot"+i).
							//	text("\u2211\u03B5 = "+formatAbs3dec(totalEmi)).
							//	style("opacity",1);
						}

						
					//add the vertical line text
					d3.select("#tooltip")			
				    .text("\u03BD = "+formatNu2dec(data.output[0][xPt].nu)+" cm⁻¹")
				    .style("opacity",1);

					//move vertical line
			    	mousex = d3.mouse(this)[0];
				    hoverLine
				 	.attr("x1",mousex)
					.attr("x2",mousex)
					.style("opacity",1);
					}  //if plotData is not null
				})
		  		
			numPlots++;

			//on page load, put up the fancy picture
			if(firstPlot==0){
				displayIntroGroup(introGroup,width,height);
				brush.extent([1061.5,1062.9]);
				svg.select(".brush").call(brush);
				brushed();
			}
			if(firstPlot>0){

				if(simType=='hitran'){
				$.cookie("spectraPlotParametersEmission_hitran", [T,P,L,vstart,vend,deltav,xArrayTotal,speciesArrayHTML]);
				}else{
				$.cookie("spectraPlotParametersEmission_nist", [T,P,L,vstart,vend,deltav,gammaTo,n,Telec,xArrayTotal,speciesArrayHTML]);
				}

				buildTables(data);
				$(document.body).animate({
		    		'scrollTop':   $('#topofplot').offset().top
					}, 500);	
				}
			
			firstPlot++;
			
			if(numPlots>=3){
				btn_hitran.button('disabled');
	 			btn_nist.button('disabled');
	 		}
			
			
			//now set lambda x-axis
			lowLambda = 10000/d3.min(x.domain());
			highLambda = 10000/d3.max(x.domain());
			
			xtop.domain([lowLambda, highLambda]);
			topaxis.call(xAxisTop);
			rightaxis.call(yAxisRight);
			brushed();
		}, //close success

	error: function(jqXHR, textStatus, errorThrown) {
		if(textStatus==="timeout") {
			$('#timeoutDiv').show();    
		    } 
		else{
			$('#alertDiv').show();
			}
		},
//	timeout: 30000
				});  //close ajax call
    		};  //close submit_form
    
    
var clear_plot = function(e) {    
    focus.selectAll(".line").remove();
    context.selectAll(".line").remove();
    numPlots = 0;
    d3.selectAll(".brush").call(brush.clear());
    legend.selectAll(".labels").remove();
    legend.selectAll(".emissionIntIndicator").remove();
    lineStrings = [];
    
    linesPer = [0,0,0];
    deltavarr = [0,0,0];
    btn_hitran.button('reset');
	btn_nist.button('reset');
	introGroup.attr("visibility","hidden");
    $('.alert-dismissible').hide();
}


function brushed() {
	// get domain
	x.domain(brush.empty() ? x2.domain() : brush.extent());
	var lineCount = 0;
	for (i=0;i<3;i++){  //for each plot
		var num2count = linesPer[i];
		for (j=0;j<num2count;j++){
			lineData = 	focus.selectAll(".line").data()[lineCount];
			lineCount++;
			var dataSum = 0;
			var scaledEmi = 0;
			var dataFiltered = lineData.filter(function(d, k) {
    			if ((d.nu >= d3.min(x.domain())) && (d.nu <= d3.max(x.domain())))  {
    				scaledEmi = +deltavarr[i]*d.abs;
    				dataSum += scaledEmi;
    		}
    	});
			legend.select("#emissionInt"+(+j+i*3))
								.text("\u222B\u03B5 = "+formatAbs3dec(dataSum)+"\u03BCW/sr-cm²")
								.style("opacity",1)
								.style("fill", colors[+j]);
		}
	}

	// if y zoom box checked, autozoom y
	if ($('input[name="autoscaleY"]').is(':checked')) {	
		var data = [];
	    for (i in focus.selectAll(".line").data()) {
    		data = data.concat(focus.selectAll(".line").data()[i]);
	    }

    var dataFiltered = data.filter(function(d, i) {
    	if ( (d.nu >= x.domain()[0]) && (d.nu <= x.domain()[1]) ) {
      		return d.abs;
    		}
		})
  	y.domain([0, 1.05*d3.max(dataFiltered.map(function(d) { return d.abs; }))]);
  }
  else { 
  		if($('input[name="fixY"]').is(':checked')) {
  		y.domain([0,$('input[name="fixYval"]').val()]);
  		}
  		else{y.domain(ylimStore);}
  		}

  var newticks = x.ticks(5);
  		for (var i = 0;i<newticks.length;i++){
  			newticks[i] = 10000/newticks[i];
  			}
		xAxisTop.tickValues(newticks);
				
  focus.selectAll(".line").attr("d", line).style("clip-path","url(#clip)");
  focus.select(".x.axis").call(xAxis);
  focus.select(".y.axis").call(yAxis);
  rightaxis.call(yAxisRight);
  lowLambda = 10000/d3.min(x.domain());
  highLambda = 10000/d3.max(x.domain());
			
  xtop.domain([lowLambda, highLambda]);
  topaxis.call(xAxisTop);
}

function save_data() {
	lineobj= {};
	focus.selectAll(".line").data().forEach(function(infoArray, index){
		lineobj['line'+index]=focus.selectAll(".line").data()[index];
	});
	var url = $SCRIPT_ROOT + '/_saveCSV';

	// hide the div, write the form
	$('#downloaddiv').hide()
    	.html('<form id="exportform" action="' + url + '" method="post">'
        + '<textarea name="data">' + JSON.stringify(lineobj) +'</textarea>'
        + '<textarea name="conditions">' + lineStrings +'</textarea>'
        + '</form>');
	// submit the form
	$('#exportform').submit();
	}

function autoscaleYclick() {
	$('input[name="fixY"]').attr('checked',false);
	brushed();
	}
	
function fixYscale() {
	$('input[name="autoscaleY"]').attr('checked',false);
	brushed();
	}	

function cm2um() {
	var cm = $('input[name="cmCalc"]').val();
	var um = 10000/cm;	
	$('input[name="umCalc"]').val(d3.round(um,5));
	}

function um2cm() {
	var um = $('input[name="umCalc"]').val();
	var cm = 10000/um;
	$('input[name="cmCalc"]').val(d3.round(cm,3));		
	}
function startcm2um() {
	var cm = $('input[name="lstart_nist"]').val();
	var um = 10000/cm;	
	$('input[name="vstart_nist"]').val(d3.round(um,5));
	var cm = $('input[name="lstart_hitran"]').val();
	var um = 10000/cm;	
	$('input[name="vstart_hitran"]').val(d3.round(um,5));
	}	

function endcm2um() {
	var cm = $('input[name="lend_nist"]').val();
	var um = 10000/cm;	
	$('input[name="vend_nist"]').val(d3.round(um,5));
	var cm = $('input[name="lend_hitran"]').val();
	var um = 10000/cm;	
	$('input[name="vend_hitran"]').val(d3.round(um,5));
	}	
	
function startum2cm() {
	var cm = $('input[name="vstart_nist"]').val();
	var um = 10000/cm;	
	$('input[name="lstart_nist"]').val(d3.round(um,5));
	var cm = $('input[name="vstart_hitran"]').val();
	var um = 10000/cm;	
	$('input[name="lstart_hitran"]').val(d3.round(um,5));
	}	

function endum2cm() {
	var cm = $('input[name="vend_nist"]').val();
	var um = 10000/cm;	
	$('input[name="lend_nist"]').val(d3.round(um,5));
	var cm = $('input[name="vend_hitran"]').val();
	var um = 10000/cm;	
	$('input[name="lend_hitran"]').val(d3.round(um,5));
	}				

$('#autoscaleY').bind('click', autoscaleYclick);
$('#fixY').bind('click', fixYscale);
$('#fixYval').bind('change', fixYscale);
$("button#calculate_hitran").bind("click",{simType: "hitran"}, submit_form);
$("button#calculate_nist").bind("click",{simType: "nist"}, submit_form);
$('button#clear').bind('click', clear_plot);
$('button#data').bind('click', save_data);
$('input[name=T]').focus();

$('input[name=lstart_nist]').bind('keyup',startcm2um);
$('input[name=lend_nist]').bind('keyup',endcm2um);

$('input[name=vstart_nist]').bind('keyup',startum2cm);
$('input[name=vend_nist]').bind('keyup',endum2cm);

$('input[name=lstart_hitran]').bind('keyup',startcm2um);
$('input[name=lend_hitran]').bind('keyup',endcm2um);

$('input[name=vstart_hitran]').bind('keyup',startum2cm);
$('input[name=vend_hitran]').bind('keyup',endum2cm); 

d3.select("#save_as_png").on("click", function(){
    saveSvgAsPng(document.getElementById("plot"), "spectraplot.png",{scale: 2});
    });





//update dropdown text
$(".hitran_species").click(function(){
  var selText = $(this).text();
  var selHTML = $(this).html();

  if(selText=="None"){
  		selText="Species";
  		selHTML="Species";}
  
  $(this).parents('.spectable').find(".specspan").html(selHTML);
  $(this).parents('.spectable').find('.specspan').find('.db').show();	
});

$('.nist_ion').click(function(e) {
        e.stopPropagation(); //This will prevent the event from bubbling up and close the dropdown when you type/click on text boxes.
    });

$('.ionstate').find('.btn').click(function(e)  {
$(this).addClass("active");
$(this).siblings(".btn").removeClass("active");
});

$('.nist_species').click(function(){
  var selText = $(this).text();
  var selHTML = $(this).html();
  ionNum = $(this).parents('.btn-group').find('.btn.active').text();  

	if(selText=="None"){
  		selText="Species";
  		selHTML="Species";
  	}

	if(ionNum=='X' || selText=="Species"){
  		$(this).parents('.spectable').find(".specspan").html(selHTML);
  	}
  	else if(ionNum=='X+'){
  		$(this).parents('.spectable').find(".specspan").html(selHTML+"<sup>+</sup>");
  	}
  	else if(ionNum='X2+'){
  		$(this).parents('.spectable').find(".specspan").html(selHTML+"<sup>2+</sup>");
  		if(selText=="H"){ // catch for Hydrogen 2+
  		$(this).parents('.spectable').find(".specspan").html(selHTML+"<sup>+</sup>");
  		}
  	} 
});



// cookie operations, setting form...
if (typeof $.cookie('spectraPlotParametersEmission_hitran') === 'undefined'){
	//no cookie
} else {
	//have cookie
	//$.cookie("spectraPlotParameters", [T,P,L,vstart,vend,deltav,xArrayTotal,speciesArrayHTML]);
	cookieArray = $.cookie('spectraPlotParametersEmission_hitran').split(",");
	$('input[name="T_hitran"]').val(cookieArray[0]); //T
	$('input[name="P_hitran"]').val(cookieArray[1]); //P
	$('input[name="L_hitran"]').val(cookieArray[2]); //L
	$('input[name="vstart_hitran"]').val(cookieArray[3]) //vstart;
	$('input[name="vend_hitran"]').val(cookieArray[4]);  //vend
	$('input[name="deltav_hitran"]').val(cookieArray[5]);//deltav
	
	$('input[name="xspecies1_hitran"]').val(cookieArray[6]);
	$('#xspec1_hitran').html(cookieArray[9]);
	$('#specspan1_hitran').html(cookieArray[9]);
	
	$('input[name="xspecies2_hitran"]').val(cookieArray[7]);
	$('#xspec2_hitran').html(cookieArray[10]);
	$('#specspan2_hitran').html(cookieArray[10]);

	$('input[name="xspecies3_hitran"]').val(cookieArray[8]);
	$('#xspec3_hitran').html(cookieArray[11]);
	$('#specspan3_hitran').html(cookieArray[11]);
}

if (typeof $.cookie('spectraPlotParametersEmission_nist') === 'undefined'){
	//no cookie
} else {
	//have cookie
	//$.cookie("spectraPlotParametersAbsorption_nist", [T_nist,P_nist,L_nist,vstart_nist,vend_nist,deltav_nist,xArrayTotal_nist,speciesArrayHTML_nist,gammaTo,n]);
	cookieArray = $.cookie('spectraPlotParametersEmission_nist').split(",");
	$('input[name="T_nist"]').val(cookieArray[0]); //T
	$('input[name="P_nist"]').val(cookieArray[1]); //P
	$('input[name="L_nist"]').val(cookieArray[2]); //L
	$('input[name="vstart_nist"]').val(cookieArray[3]) //vstart;
	$('input[name="vend_nist"]').val(cookieArray[4]);  //vend
	$('input[name="deltav_nist"]').val(cookieArray[5]);//deltav
	$('input[name="gammaTo"]').val(cookieArray[6]);  //vend
	$('input[name="n"]').val(cookieArray[7]);//deltav
	$('input[name="Telec_nist"]').val(cookieArray[8]);//deltav
	
	$('input[name="xspecies1_nist"]').val(cookieArray[9]);
	$('#xspec1_nist').html(cookieArray[12]);
	$('#specspan1_nist').html(cookieArray[12]);
	
	$('input[name="xspecies2_nist"]').val(cookieArray[10]);
	$('#xspec2_nist').html(cookieArray[13]);
	$('#specspan2_nist').html(cookieArray[13]);

	$('input[name="xspecies3_nist"]').val(cookieArray[11]);
	$('#xspec3_nist').html(cookieArray[14]);
	$('#specspan3_nist').html(cookieArray[14]);
}

startum2cm();
endum2cm();
$("button#calculate_hitran").click();

});



  
