function displayIntroGroup(introGroup,width,height) {
				introGroup.append("rect")
					.attr("fill","white")
				    .attr("width", width)
				    .attr("height", height)
				    .attr("opacity",0.5);
			  
				introGroup.append("text")
				  .text("Enter your simulation parameters up there")
				  .attr("font-size","15px")
				  .attr("transform", "translate(250,60)")
				  .attr("fill","black")
				  .attr("font-family", "sans-serif")
	    		  .attr("font-size", "15px");			   			  			   			  

				introGroup.append("path")
				  .attr("stroke-width","2")
				  .attr("marker-end","url(#arrowhead)")
				  .attr("fill","none")
				  .attr("stroke","black")
				  .attr("d","M540,57 C540,57 570,55 570,5");
			  
				introGroup.append("text")
				  .text("Click Calculate to run the simulation")
				  .attr("font-size","15px")
				  .attr("transform", "translate(320,110)")
				  .attr("fill","black")
				  .attr("font-family", "sans-serif")
	    		  .attr("font-size", "15px");
			  
				introGroup.append("path")
				  .attr("stroke-width","2")
				  .attr("marker-end","url(#arrowhead)")
				  .attr("fill","none")
				  .attr("stroke","black")
				  .attr("d","M570,107 C570,107 620,130 750,20")
			  
				introGroup.append("text")
				  .text("Click and drag on the bottom plot to pan and stretch to zoom")
				  .attr("font-size","15px")
				  .attr("transform", "translate(280,170)")
				  .attr("fill","black")
				  .attr("font-family", "sans-serif")
	    		  .attr("font-size", "15px");			  

				introGroup.append("path")
				  .attr("stroke-width","2")
				  .attr("marker-end","url(#arrowhead)")
				  .attr("fill","none")
				  .attr("stroke","black")
				  .attr("d","M420,180 C420,220 600,250 475,310")	
				}