$(function() {

////////////////////////////////			
// function on submit
var submit_form = function(e){

	
	var btn = $('#calculate');
    btn.button('loading');
    
    
	vstart = $('input[name="vstart"]').val();
	vend = $('input[name="vend"]').val();
	cook   = $.cookie('_ga');
	
	
	
	
/////// Here's the AJAX call to the python script	
       $.ajax({
		 url:	$SCRIPT_ROOT + '/_calcHW',
		data: {
			vstart: vstart,
            vend: vend,
            cook: cook},

	 success: function(data) { 
				btn.button('reset');

				$.cookie("spectraPlotHardware", [vstart,vend]);
				buildTables(data);
		}, //close success

	error: function(jqXHR, textStatus, errorThrown) {
		if(textStatus==="timeout") {
			$('#timeoutDiv').show();    
		    } 
		else{
			$('#alertDiv').show();
			}
		},
//	timeout: 30000
				});  //close ajax call
    		};  //close submit_form
    
    
var clear_plot = function(e) {    
    
    var btn = $('#calculate');
    btn.button('reset');
    $('#timeoutDiv').hide();
    $('#molefracDiv').hide();  
    $('#alertDiv').hide();  
    $('#reduceDiv').hide();  
    $('#simrangeDiv').hide(); 
    $('#notinfraredDiv').hide();

}

	
function startcm2um() {
	var cm = $('input[name="lambdastart"]').val();
	var um = 10000/cm;	
	$('input[name="vstart"]').val(d3.round(um,5));
	}	

function endcm2um() {
	var cm = $('input[name="lambdaend"]').val();
	var um = 10000/cm;	
	$('input[name="vend"]').val(d3.round(um,5));
	}	
	
function startum2cm() {
	var cm = $('input[name="vstart"]').val();
	var um = 10000/cm;	
	$('input[name="lambdastart"]').val(d3.round(um,5));
	}	

function endum2cm() {
	var cm = $('input[name="vend"]').val();
	var um = 10000/cm;	
	$('input[name="lambdaend"]').val(d3.round(um,5));
	}		

$('button#calculate').bind('click', submit_form);
$('button#clear').bind('click', clear_plot);

$('input[name=lambdastart]').bind('keyup',startcm2um);
$('input[name=lambdaend]').bind('keyup',endcm2um);

$('input[name=vstart]').bind('keyup',startum2cm);
$('input[name=vend]').bind('keyup',endum2cm);

// cookie operations, setting form...
if (typeof $.cookie('spectraPlotHardware') === 'undefined'){
	
} else {
	//have cookie
	//$.cookie("spectraPlotParameters", [T,P,L,vstart,vend,deltav,xArrayTotal,speciesArrayHTML]);
	cookieArray = $.cookie('spectraPlotHardware').split(",");
	console.log($.cookie('spectraPlotHardware').split(","));
	$('input[name="vstart"]').val(cookieArray[0]) //vstart;
	$('input[name="vend"]').val(cookieArray[1]);  //vend
	}
startum2cm();
endum2cm();
});



  
