def calcAbs(T,P,L,vstart,vend,dv,Species_Vector,Xabs_Vector):

	import Absorbance_Code_Function_v1 as AF
	import numpy



######################################################################################################################
# User inputs
	wing_eval       = 1                   # Distance away form linecenter to stop simulation lineshape (cm^-1)
	if P<10:
		wing_eval=200.0
	elif P>100:
		wing_eval=2000.0
	else:
		wing_eval=P*20.0

	########################################################################################################################
	# Python does the rest
	# print 'mole fraction is', Xabs
	# tic             = time.time()
	v               = numpy.arange(vstart,vend,dv)
	# v               = numpy.around(v,decimals=7)
	num_species     = len(Species_Vector)
	length_v        = len(v)
	v_alpha         = numpy.zeros((length_v,num_species+1))
	v_alpha[:,0]    = v
	output			= numpy.zeros((length_v,num_species))
	outs			= numpy.zeros((length_v,num_species))
	#print output
	inputs          = Species_Vector, T, P, Xabs_Vector, L, dv, vstart, vend, wing_eval
	#output          = AF.Absorbance_Code_Function_v1([inputs,0])
	outs = {}

	
	
	for ii in range(0,num_species):
		
		
		inputs          = Species_Vector, T, P, Xabs_Vector, L, dv, vstart, vend, wing_eval
		output[:,ii]    = AF.Absorbance_Code_Function_v1([inputs,ii])  
		ans = zip(v,output[:,ii])
		ansWithKeys = [{'nu':x, 'abs':y} for x,y in ans]
		outs[ii] 		= ansWithKeys
		#print output
		#outs[ii] = zip(v,output)

	return outs

