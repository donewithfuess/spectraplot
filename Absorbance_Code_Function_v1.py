# -*- coding: utf-8 -*-
"""
Created on Sat Jul 19 16:11:27 2014

@author: Chris Goldenstein 
"""
def Absorbance_Code_Function_v1(args):
    
    import Voigt_McLean_v1 as VF
    import Linestrength_Function_v1 as SF
    import Species_Function_Term_v1 as SpF
    import numpy 
    import math
    
    inputs          = args[0]
    kk              = args[1]
    Species_Vector  = inputs[0]
    T               = inputs[1]
    P               = inputs[2]
    Xabs_Vector     = inputs[3]
    L               = inputs[4]
    dv              = inputs[5]
    vstart          = inputs[6]
    vend            = inputs[7]
    wing_eval       = inputs[8]
    Species         = Species_Vector[kk]
    Xabs            = Xabs_Vector[kk]

    
    v                                       = numpy.arange(vstart,vend,dv)      
    M, Q_To, Q_T, SpecData, num_isotopes    = SpF.Species_Function_Term_v1(Species,T,vstart,vend)

    vo      = SpecData[0,2]
    ii      = 0
    istart  = 0
    while vo < vstart-wing_eval and ii < len(SpecData[:,0]):
        vo      = SpecData[ii,2]
        ii      = ii + 1
        istart  = ii
    
    iend    = istart    
    ii      = istart
    while vo < vend+wing_eval and ii < len(SpecData[:,0]):
        vo      = SpecData[ii,2]
        ii      = ii + 1
        iend    = ii    
    
    if ii == len(SpecData[:,0]):
        iend = iend-1
    
    length  = len(v)
    alpha   = numpy.zeros(length)
    bad_iso = 0
    for ii in range(istart, iend+1):
        isotope = SpecData[ii,1]

        if isotope > num_isotopes and bad_iso==0:    
            print ('isotope out of range')

        if isotope > num_isotopes:
            isotope = num_isotopes
            bad_iso = 1
            
        vo      = SpecData[ii,2]        
        E       = SpecData[ii,7]  
        S_To    = SpecData[ii,3]
        Gair    = SpecData[ii,5]
        Gabs    = SpecData[ii,6]
        nair    = SpecData[ii,8]  
        Dair    = SpecData[ii,9]
        nabs    = 0.75    
        mair    = 0.96
        #print vo
        #print E
        #print S_To
        #print Gair
        #print Gabs
        #print nair
        #print Dair

        GairT   = Gair*(296.0/T)**nair
        GabsT   = Gabs*(296.0/T)**nabs    
        DairT   = Dair*(296.0/T)**mair

        vo      = vo + P*(1-Xabs)*DairT        
        
        S_T     = SF.Linestrength_Function_v1(vo,E,S_To,Q_To[isotope-1],Q_T[isotope-1],T)
        A       = S_T*P*Xabs*L

        vd     = vo*7.1623*10**(-7)*math.sqrt(T/M[isotope-1])
        vc     = 2*P*(Xabs*GabsT + (1-Xabs)*GairT)

        phiV   = VF.Voigt_McLean_v1(v, vo, vd, vc, dv, wing_eval)     
        alpha  = A*phiV + alpha
    #print alpha
    return alpha




